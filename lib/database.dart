import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseMethods {
  getUserByUsername(String username) async {
    return await FirebaseFirestore.instance
        .collection("users")
        .where("name", isEqualTo: username)
        .get();
  }

  getUserThroughSearch(String strSearch, bool isStudent) async {
    strSearch = strSearch.toLowerCase();
    var strFrontCode;
    var strEndCode;
    var startcode;
    var endcode;
    if (strSearch.length > 1) {
      strFrontCode = strSearch.substring(0, strSearch.length - 1);
      strEndCode = strSearch.substring(strSearch.length - 1, strSearch.length);
      startcode = strSearch;
      endcode =
          strFrontCode + String.fromCharCode(strEndCode.codeUnitAt(0) + 1);
      return await FirebaseFirestore.instance
          .collection("users")
          .where("nameToLower", isGreaterThanOrEqualTo: startcode)
          .where("nameToLower", isLessThan: endcode)
          .where("instructor", isEqualTo: !isStudent)
          .get();
    } else if (strSearch.length == 1) {
      strFrontCode = strSearch.substring(0);
      startcode = strSearch;
      endcode = String.fromCharCode(strFrontCode.codeUnitAt(0) + 1);
      return await FirebaseFirestore.instance
          .collection("users")
          .where("nameToLower", isGreaterThanOrEqualTo: startcode)
          .where("nameToLower", isLessThan: endcode)
          .where("instructor", isEqualTo: !isStudent)
          .get();
    }
  }

  getUserByUserEmail(String userEmail) async {
    return await FirebaseFirestore.instance
        .collection("users")
        .where("email", isEqualTo: userEmail)
        .get();
  }

  getUserByUserEmailStream(String userEmail) async {
    return FirebaseFirestore.instance
        .collection("users")
        .where("email", isEqualTo: userEmail)
        .snapshots();
  }

  getInstructorVerificationPin() async {
    return await FirebaseFirestore.instance
        .collection("admin")
        .doc("instructorPin")
        .get();
  }

  getFilteredInstructors(String state, String grade, String instruments) async {
    var result = FirebaseFirestore.instance
        .collection("users")
        .where("instructor", isEqualTo: true)
        .where("available", isEqualTo: true);

    List<String> instrumArr = instruments.split(", ");

    for (int i = 0; i < instrumArr.length; i++) {
      instrumArr[i] = instrumArr[i].toLowerCase();
    }

    if (state != 'Any') result = result.where("state", isEqualTo: state);
    if (grade != '') result = result.where("grade_level", isEqualTo: grade);
    if (instruments != '')
      result = result.where("instrumParse", arrayContainsAny: instrumArr);

    return result.get();
  }

  uploadUserInfo(userMap) {
    FirebaseFirestore.instance.collection("users").add(userMap);
  }

  updateUserInfo(docID, userMap) {
    return FirebaseFirestore.instance
        .collection("users")
        .doc(docID)
        .update(userMap);
  }

  createChatRoom(String chatRoomId, chatRoomMap) {
    FirebaseFirestore.instance
        .collection("ChatRoom")
        .doc(chatRoomId)
        .set(chatRoomMap)
        .catchError((e) {
      // print(e.toString());
    });
  }

  addConversationMessages(String chatRoomId, messageMap) {
    FirebaseFirestore.instance
        .collection("ChatRoom")
        .doc(chatRoomId)
        .collection("chats")
        .add(messageMap)
        .catchError((e) {
      // print(e.toString());
    });
  }

  doesChatRoomExist(String chatRoomId) async {
    var doc = await FirebaseFirestore.instance
        .collection("ChatRoom")
        .doc(chatRoomId)
        .get();
    return doc.exists;
  }

  getConversationMessages(String chatRoomId) async {
    return FirebaseFirestore.instance
        .collection("ChatRoom")
        .doc(chatRoomId)
        .collection("chats")
        .orderBy("time", descending: false)
        .snapshots();
  }

  updateLastMessageSend(String chatRoomId, Map lastMessageInfoMap) {
    return FirebaseFirestore.instance
        .collection("ChatRoom")
        .doc(chatRoomId)
        .update(lastMessageInfoMap);
  }

  getChatRooms(String userEmail) async {
    // Updates with new chatrooms
    return FirebaseFirestore.instance
        .collection("ChatRoom")
        .where("usersID", arrayContains: userEmail)
        .orderBy("lastMessageSendTime", descending: true)
        .snapshots();
  }

  getChatRoomList(String userEmail) {
    // Does not update with new chatrooms
    return FirebaseFirestore.instance
        .collection("ChatRoom")
        .where("usersID", arrayContains: userEmail)
        .get();
  }

  getLessonsStream(String user, bool student, bool desc) async {
    if (student) {
      return FirebaseFirestore.instance
          .collection("lessons")
          .where("student", isEqualTo: user)
          .where('status', isEqualTo: "Upcoming")
          // .orderBy('startTime', descending: desc)
          .snapshots();
    } else {
      return FirebaseFirestore.instance
          .collection("lessons")
          .where("instructor", isEqualTo: user)
          .where('status', isEqualTo: "Upcoming")
          // .orderBy('startTime', descending: desc)
          .snapshots();
    }
  }

  getLessonsUser(
      String user, bool student, bool descending, bool stream) async {
    var snapshot;
    if (student == true) {
      snapshot = FirebaseFirestore.instance
          .collection("lessons")
          .where('student', isEqualTo: user)
          .orderBy('startTime', descending: descending);
    } else {
      snapshot = FirebaseFirestore.instance
          .collection("lessons")
          .where('instructor', isEqualTo: user)
          .orderBy('startTime', descending: descending);
    }

    if (stream)
      return snapshot.snapshots();
    else
      return snapshot.get();
  }

  getLessons(String instructor, String student) {
    return FirebaseFirestore.instance
        .collection("lessons")
        .where('instructor', isEqualTo: instructor)
        .where('student', isEqualTo: student)
        .orderBy('startTime', descending: true)
        .get();
  }

  addLesson(lessonMap) {
    FirebaseFirestore.instance
        .collection("lessons")
        .add(lessonMap)
        .catchError((e) {
      // print(e.toString());
    });
  }

  updateLesson(String lessonID, userMap) {
    return FirebaseFirestore.instance
        .collection("lessons")
        .doc(lessonID)
        .update(userMap);
  }

  deleteLesson(String lessonID) {
    FirebaseFirestore.instance.collection("lessons").doc(lessonID).delete();
  }
}
