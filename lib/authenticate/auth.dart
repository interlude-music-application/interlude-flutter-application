import 'package:firebase_auth/firebase_auth.dart';
import 'package:interlude_music_app/models/theUser.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create TheUser obj based on User
  TheUser _fromFirebaseUser(User user) {
    return user != null ? TheUser(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<TheUser> get user {
    return _auth
        .authStateChanges()
        //.map((User user) => _fromFirebaseUser(user));
        .map(_fromFirebaseUser);
  }

  // sign in anon - for testing
  Future signInAnon() async {
    try {
      UserCredential userCredential = await _auth.signInAnonymously();
      User user = userCredential.user;
      return _fromFirebaseUser(user);
    } catch (e) {
      // print(e.toString());
      return null;
    }
  }

  // sign in with email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = userCredential.user;
      return _fromFirebaseUser(user);
    } catch (e) {
      // print(e.toString());
      return null;
    }
  }

  // register with email & password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);

      User user = userCredential.user;
      return _fromFirebaseUser(user);
    } catch (e) {
      // print(e.toString());
      return null;
    }
  }

  Future sendEmailVerification() async {
    User user = _auth.currentUser;
    if (user != null) {
      // if (!user.emailVerified) {
      await user.sendEmailVerification();
      // }
    } else {
      return null;
    }
  }

  getCurrentUser() {
    return FirebaseAuth.instance.currentUser;
  }

  // sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      // print(e.toString());
      return null;
    }
  }
}
