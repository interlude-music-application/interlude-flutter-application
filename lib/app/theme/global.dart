import 'package:flutter/material.dart';

TextStyle primaryText =
    new TextStyle(fontFamily: 'Monsterrat', color: Colors.black, fontSize: 16);

AppBar primaryBar(String titleText, bool implyLeading) {
  return AppBar(
    title: Text(
      titleText,
      style: primaryText,
    ),
    centerTitle: true,
    backgroundColor: Colors.white,
    iconTheme: IconThemeData(color: Colors.black),
    automaticallyImplyLeading: implyLeading,
  );
}

Color getColor(String status) {
  switch (status) {
    case "Upcoming":
      return Color(0xFF8A8A8E);
      break;
    case "Canceled":
      return Color(0xFFE74141);
      break;
  }
  return Color(0xFF6E6DB4);
}

// generate unique id for every chat room
getChatRoomId(String a, String b) {
  if (a.compareTo(b) < 0) {
    return "$a $b";
  } else {
    return "$b $a";
  }
}
