import 'package:flutter/cupertino.dart';

Widget customText(String custom) {
  return Container(
      padding: EdgeInsets.fromLTRB(15, 0, 0, 2), child: Text(custom));
}
