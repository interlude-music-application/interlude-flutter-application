import 'package:flutter/material.dart';
import 'package:interlude_music_app/models/theUser.dart';
import 'package:provider/provider.dart';

import 'screens/home.dart';
import 'screens/landingPage.dart';

class AuthCheck extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<TheUser>(context);
    // print(user);

    if (user == null) {
      return MaterialApp(
          theme: ThemeData(fontFamily: 'Montserrat'),
          initialRoute: '/',
          routes: {'/': (context) => LandingPage()});
    } else {
      return Home();
    }
  }
}
