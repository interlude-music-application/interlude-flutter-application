import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:interlude_music_app/authenticate/auth.dart';
import 'package:interlude_music_app/models/theUser.dart';
import 'package:provider/provider.dart';

import 'authCheck.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(StreamProvider<TheUser>.value(
      initialData: null,
      value: AuthService().user,
      child: MaterialApp(
        home: AuthCheck(),
      ),
    ));
  });
}
