import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/screens/lessons.dart';
import 'package:interlude_music_app/screens/single_chat.dart';

class NotificationService {
  String userEmail;
  BuildContext context;
  bool isInstructor;
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  NotificationService(this.userEmail, this.context, this.isInstructor);

  static const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.max,
  );

  initialize() async {
    final DatabaseMethods databaseMethods = new DatabaseMethods();
    List<String> tokens;
    bool newToken = true;

    messaging.requestPermission();

    String token = await messaging.getToken();

    databaseMethods.getUserByUserEmail(userEmail).then((val) {
      try {
        tokens = List.from(val.docs[0].data()["tokens"]);
      } catch (error) {
        tokens = null;
      }

      if (tokens != null) {
        for (var i = 0; i < tokens.length; i++) {
          if (token == tokens[i]) newToken = false;
        }
      }

      // print(token);

      //add new token if not found
      if (newToken) {
        // print("new token for " + userEmail + "!");
        databaseMethods.getUserByUserEmail(userEmail).then((doc) {
          databaseMethods.updateUserInfo(doc.docs[0].id, {
            "tokens": FieldValue.arrayUnion([token])
          });
        });
        HelperFunctions.saveUserTokenSharedPreference(token);
      }
    });

    Future selectNotification(String payload) async {
      // print(payload);
      Map<String, dynamic> data = json.decode(payload);

      // print("test");

      if (data['type'] == 'chat') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SingleChat(data['receivedByEmail'],
                    data['sendByEmail'], data['sendByName'])));
      } else if (data['type'] == 'lessonStatus') {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Lessons()));
      }
    }

    // ios request notifications

    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('ic_launcher');

    // Initialize iOS settings

    final InitializationSettings initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;

      if (notification != null && android != null) {
        databaseMethods.getUserByUserEmail(userEmail).then((val) {
          // Cancel chat notifications if the user is currently chatting with that person
          bool cancelNotification = false;
          String chattingWith = val.docs[0].data()["chattingWith"];
          if (message.data["type"] == "chat" &&
              message.data["sendByEmail"] == chattingWith) {
            cancelNotification = true;
          }

          if (!cancelNotification) {
            flutterLocalNotificationsPlugin.show(
              notification.hashCode,
              notification.title,
              notification.body,
              NotificationDetails(
                android: AndroidNotificationDetails(
                  channel.id,
                  channel.name,
                  channel.description,
                ),

                // TODO: add iOS
              ),
              payload: json.encode(message.data),
            );
          }
        });
      }
    });

    // Subscribe users to topics
    // Send topic notifications through the Firebase Cloud Messaging service
    // under the firebase console: https://console.firebase.google.com/u/0/project/interlude-music-app/overview
    await FirebaseMessaging.instance.subscribeToTopic('general');

    if (isInstructor) {
      await FirebaseMessaging.instance.subscribeToTopic('instructors');
    } else {
      await FirebaseMessaging.instance.subscribeToTopic('students');
    }
  }
}
