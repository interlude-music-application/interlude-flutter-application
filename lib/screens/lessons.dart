import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:intl/intl.dart';

import 'bottom_nav.dart';
import 'main_drawer.dart';
import 'search_user.dart';

class Lessons extends StatefulWidget {
  @override
  _LessonsState createState() => _LessonsState();
}

class _LessonsState extends State<Lessons> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController searchTextEditingController =
      new TextEditingController();

  String _userID = "";
  bool _isStudent = false;
  bool _cancelModeOn = false;
  Stream lessonsStream;
  Color _iconColor = Color.fromRGBO(159, 155, 155, 1);

  getLessons() {
    databaseMethods.getLessonsStream(_userID, _isStudent, true).then((val) {
      if (mounted) {
        setState(() {
          lessonsStream = val;
        });
      }
    });
  }

  Widget lessonList() {
    return StreamBuilder(
      stream: lessonsStream,
      builder: (context, snapshot) {
        return snapshot.hasData && snapshot.data.docs.length > 0
            ? ListView.builder(
                itemCount: snapshot.data.docs.length,
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (content, index) {
                  List docs = snapshot.data.docs;
                  docs.sort((a, b) => b
                      .data()["startTime"]
                      .toDate()
                      .toString()
                      .compareTo(a.data()["startTime"].toDate().toString()));
                  var data = docs[docs.length - 1 - index].data();
                  String date =
                      '${DateFormat.yMMMd().format(data["startTime"].toDate())}';
                  String time =
                      '${DateFormat.jm().format(data["startTime"].toDate())}-${DateFormat.jm().format(data["endTime"].toDate())} ${data["startTime"].toDate().timeZoneName}';

                  return _isStudent
                      ? LessonTile(
                          name: data["instructor_name"],
                          date: date,
                          timeRange: time,
                          status: data["status"])
                      : new GestureDetector(
                          onTap: () {
                            if (_cancelModeOn) {
                              _cancelConfirmation(
                                date,
                                time,
                                docs[docs.length - 1 - index].id,
                              );
                            }
                          },
                          child: LessonTile(
                              name: data["student_name"],
                              date: date,
                              timeRange: time,
                              status: data["status"]));
                })
            : Center(
                child: Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: Text("No Upcoming Lessons",
                        style: TextStyle(
                            fontFamily: 'Monsterrat',
                            color: Colors.black,
                            fontSize: 24,
                            fontWeight: FontWeight.bold))));
      },
    );
  }

  Future<void> _cancelDirections() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Cancel Lesson'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Tap the lesson you want to cancel.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _cancelConfirmation(
      String date, String time, String lessonID) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Cancel Lesson'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure you want to cancel the lesson on ' +
                    date +
                    ' from ' +
                    time +
                    '?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Yes'),
              onPressed: () {
                _cancelModeOn = false;
                if (mounted) {
                  setState(() {
                    _iconColor = Color.fromRGBO(159, 155, 155, 1);
                  });
                }
                Map<String, dynamic> statusMap = {"status": "Canceled"};
                databaseMethods.updateLesson(lessonID, statusMap);
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('No'),
              onPressed: () {
                if (mounted) {
                  setState(() {
                    _iconColor = Color.fromRGBO(159, 155, 155, 1);
                  });
                }
                _cancelModeOn = false;
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Lessons',
          style: primaryText,
        ),
        actions: !_isStudent
            ? <Widget>[
                Container(
                  padding: EdgeInsets.only(right: 3.0),
                  child: IconButton(
                      icon: Icon(
                        Icons.add,
                        color: Colors.blue[800],
                        size: 30,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SearchUser()));
                      }),
                ),
                Container(
                  padding: EdgeInsets.only(right: 3.0),
                  child: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: _iconColor,
                        size: 30,
                      ),
                      onPressed: () {
                        _cancelModeOn = !_cancelModeOn;
                        if (mounted) {
                          setState(() {
                            if (_cancelModeOn) {
                              _iconColor = Colors.blue[800];
                              _cancelDirections();
                            } else
                              _iconColor = Color.fromRGBO(159, 155, 155, 1);
                          });
                        }
                      }),
                )
              ]
            : [],
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        automaticallyImplyLeading: true,
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [lessonList()],
        ),
      ),
      bottomNavigationBar: BottomNav(0, false),
    );
  }

  @override
  void initState() {
    getUserID();
    super.initState();
  }

  void getUserID() async {
    String id = await HelperFunctions.getUserEmailSharedPreference();
    if (mounted) {
      setState(() {
        _userID = id;
        isStudent();
      });
    }
  }

  void isStudent() async {
    bool isInstructor =
        await HelperFunctions.getUserIsInstructorSharedPreference();
    if (mounted) {
      setState(() {
        _isStudent = !isInstructor;
        getLessons();
      });
    }
  }
}

class LessonTile extends StatelessWidget {
  final String name;
  final String date;
  final String timeRange;
  final String status;

  LessonTile({this.name, this.date, this.timeRange, this.status});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                date ?? '',
                style: TextStyle(
                    fontFamily: 'Monsterrat',
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              //It would be nice to make this cleaner but dateformat can't handle timezones
              Text(timeRange ?? '',
                  style: TextStyle(
                      fontFamily: 'Monsterrat',
                      color: Colors.black,
                      fontSize: 16))
            ],
          ),
          Expanded(
            child: Container(color: Colors.white),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name ?? '',
                style: TextStyle(
                    fontFamily: 'Monsterrat',
                    color: getColor(status),
                    fontSize: 16),
              ),
              Text(
                status ?? '',
                style: TextStyle(
                    fontFamily: 'Monsterrat',
                    color: getColor(status),
                    fontSize: 16),
              )
            ],
          )
        ],
      ),
    );
  }
}
