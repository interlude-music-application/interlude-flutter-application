import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/constants.dart';
import 'package:interlude_music_app/database.dart';
import 'package:intl/intl.dart';

class SingleChat extends StatefulWidget {
  final String sendByEmail;
  final String receivedByEmail;
  final String receivedByName;
  SingleChat(this.sendByEmail, this.receivedByEmail, [this.receivedByName]);

  @override
  _SingleChatState createState() => _SingleChatState();
}

class _SingleChatState extends State<SingleChat> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController messageController = new TextEditingController();

  Stream chatMessagesStream;
  String chatroomId, sendByID, receivedByID;
  var unreadChats = 0;

  Widget chatMessageList() {
    return StreamBuilder(
      stream: chatMessagesStream,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                reverse: true,
                itemCount: snapshot.data.docs.length,
                itemBuilder: (context, index) {
                  return MessageTile(
                    snapshot.data.docs[snapshot.data.docs.length - index - 1]
                        .data()["message"],
                    snapshot.data.docs[snapshot.data.docs.length - index - 1]
                            .data()["sendBy"] ==
                        Constants.myName,
                    DateTime.fromMillisecondsSinceEpoch(snapshot
                            .data.docs[snapshot.data.docs.length - index - 1]
                            .data()["time"])
                        .toLocal(),
                  );
                })
            : Container();
      },
    );
  }

  sendMessage() {
    String receivedByName = "";
    List receiverTokens = [];

    databaseMethods.getUserByUserEmail(widget.receivedByEmail).then((val) {
      if (val.docs.length == 0) return;
      if (val.docs[0].data()["tokens"] != null)
        receiverTokens = List.from(val.docs[0].data()["tokens"]);
      if (val.docs[0].data()["name"] != null)
        receivedByName = val.docs[0].data()["name"];

      if (messageController.text.isNotEmpty) {
        Map<String, dynamic> messageMap = {
          "message": messageController.text,
          "sendBy": Constants.myName,
          "sendByID": widget.sendByEmail,
          "receivedBy": receivedByName,
          "receivedByID": widget.receivedByEmail,
          "receivedByTokens": receiverTokens,
          "time": DateTime.now().millisecondsSinceEpoch
        };
        databaseMethods.addConversationMessages(chatroomId, messageMap);

        Map<String, dynamic> lastMessageInfoMap = {
          "lastMessage": messageController.text,
          "lastMessageUnread": true,
          "lastMessageSendTime": DateTime.now(),
          "lastMessageSendBy": Constants.myName,
          "lastMessageSendToID": widget.receivedByEmail,
          "lastMessageSendByID": widget.sendByEmail
        };

        databaseMethods.updateUserInfo(receivedByID, {"unreadMessage": true});

        databaseMethods.updateLastMessageSend(chatroomId, lastMessageInfoMap);

        messageController.text = "";
      }
    });
  }

  updateUnread() {
    Map<String, dynamic> updateUnread = {
      "lastMessageUnread": false,
    };

    // print("wrote to the database!");
    databaseMethods.updateLastMessageSend(chatroomId, updateUnread);

    databaseMethods.getChatRoomList(Constants.myEmail).then((snapshot) {
      if (mounted) {
        setState(() {
          var _unreadChats = 0;
          for (var i = 0; i < snapshot.docs.length; i++) {
            if (snapshot.docs[i].data()["lastMessageUnread"] == true)
              _unreadChats++;
          }
          if (_unreadChats == 0)
            databaseMethods.updateUserInfo(sendByID, {"unreadMessage": false});
        });
      }
    });
  }

  @override
  void initState() {
    chatroomId = getChatRoomId(widget.sendByEmail, widget.receivedByEmail);

    databaseMethods.getUserByUserEmail(widget.sendByEmail).then((snapshot) {
      if (mounted) {
        setState(() {
          if (snapshot == null) return;
          sendByID = snapshot.docs[0].id;
        });
        databaseMethods.updateUserInfo(sendByID, {
          "chattingWith": widget.receivedByEmail,
        });
      }
    });

    databaseMethods.getUserByUserEmail(widget.receivedByEmail).then((snapshot) {
      if (mounted) {
        setState(() {
          if (snapshot == null) return;
          receivedByID = snapshot.docs[0].id;
        });
      }
    });

    databaseMethods.getConversationMessages(chatroomId).then((value) {
      if (mounted) {
        setState(() {
          chatMessagesStream = value;
        });
      }
    });

    updateUnread();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    updateUnread();
    // print("single chat disposed");
    databaseMethods.updateUserInfo(sendByID, {
      "chattingWith": "",
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.receivedByName != null) Constants.chat = widget.receivedByName;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            Constants.chat,
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Container(
            child: Column(
          children: [
            Expanded(child: chatMessageList()),
            Container(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(color: Colors.black45, width: 1.0))),
                child: Row(children: [
                  Expanded(
                    child: TextField(
                      controller: messageController,
                      minLines: 1,
                      maxLines: 5,
                      textCapitalization: TextCapitalization.sentences,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                          hintText: "Message...", border: InputBorder.none),
                    ),
                  ),
                  SizedBox(width: 10.0),
                  GestureDetector(
                    onTap: () {
                      sendMessage();
                    },
                    child: Icon(
                      Icons.send,
                      size: 30,
                    ),
                  ),
                ]),
              ),
            ),
          ],
        )));
  }
}

class MessageTile extends StatefulWidget {
  final String message;
  final bool isSendByMe;
  final DateTime timestamp;
  MessageTile(this.message, this.isSendByMe, this.timestamp);

  @override
  _MessageTileState createState() => _MessageTileState();
}

class _MessageTileState extends State<MessageTile> {
  bool timeVisible = false;

  // Gets appropriate time string format based on how long ago it has been sent
  // (X min, or 4:00 PM, or Thurs 4:00 PM, or Jul 20, 4:00 PM, etc.)
  getFormattedTimeString(DateTime d) {
    DateTime now = DateTime.now();
    Duration difference = now.difference(d);
    if (difference.inMinutes < 1) return "Now";
    if (difference.inMinutes < 60)
      return difference.inMinutes.toString() + " min";
    if (difference.inDays < 1) return DateFormat.jm().format(d);
    if (difference.inDays < 7)
      return DateFormat.E().format(d) + " " + DateFormat.jm().format(d);
    if (difference.inDays < 365)
      return DateFormat.MMMMd().format(d) + ", " + DateFormat.jm().format(d);
    return DateFormat.MMMMd().format(d) +
        " " +
        DateFormat.y().format(d) +
        ", " +
        DateFormat.jm().format(d);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
          padding: EdgeInsets.only(
              left: widget.isSendByMe ? 90 : 15,
              right: widget.isSendByMe ? 15 : 90),
          margin: EdgeInsets.symmetric(vertical: 8),
          width: MediaQuery.of(context).size.width,
          child: IntrinsicHeight(
            child: Row(
              children: [
                if (!widget.isSendByMe)
                  Padding(
                    padding: EdgeInsets.only(right: 15),
                    child: CircleAvatar(
                        radius: 15,
                        child: Image(image: AssetImage('assets/Ellipse.png'))),
                  ),
                Expanded(
                    child: Column(
                        crossAxisAlignment: widget.isSendByMe
                            ? CrossAxisAlignment.end
                            : CrossAxisAlignment.start,
                        children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16, vertical: 14),
                          decoration: BoxDecoration(
                              color: widget.isSendByMe
                                  ? Colors.blue[300]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(23),
                                  topRight: Radius.circular(23),
                                  bottomRight: Radius.circular(23),
                                  bottomLeft: Radius.circular(23))),
                          child: SelectableText(
                            // Allows copying text
                            widget.message,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Visibility(
                          child: Text(
                            getFormattedTimeString(widget.timestamp),
                          ),
                          visible: timeVisible,
                        ),
                      ),
                    ])),
              ],
            ),
          )),
      onTap: () => {
        setState(() => {timeVisible = !timeVisible})
      },
    );
  }
}
