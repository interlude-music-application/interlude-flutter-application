import 'package:flutter/material.dart';
import 'package:interlude_music_app/screens/tos.dart';

import '../ui/sharedWidgets/customText.dart';

class CreateAccount extends StatefulWidget {
  final bool _instructor;
  const CreateAccount(this._instructor);

  @override
  _CreateAccountState createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  final _formKey = GlobalKey<FormState>();

  // text field state
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _confirmPass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Container(
          child: Image.asset('assets/interludetitleblack.png',
              fit: BoxFit.contain, scale: 5.0),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        automaticallyImplyLeading: true,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus &&
              currentFocus.focusedChild != null) {
            currentFocus.focusedChild.unfocus();
          }
        },
        child: Padding(
          padding: EdgeInsets.only(bottom: bottom),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 60.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: size.height * 0.2),
                    Center(
                      child: Text(
                        'Create Account',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(height: size.height * 0.04),
                    customText('Email Address'),
                    Container(
                      height: size.height * 0.05,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          isDense: true,
                          suffixIcon: Icon(Icons.person),
                          contentPadding: EdgeInsets.fromLTRB(15, 20, 10, 0),
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                          ),
                        ),
                        validator: (val) =>
                            val.isEmpty ? 'Enter an email' : null,
                        controller: _email,
                      ),
                    ),
                    SizedBox(height: size.height * 0.02),
                    customText('Password'),
                    Container(
                      height: size.height * 0.05,
                      child: TextFormField(
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          suffixIcon: Icon(Icons.lock),
                          contentPadding: EdgeInsets.fromLTRB(15, 20, 10, 0),
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                          ),
                        ),
                        validator: (val) => val.length < 6
                            ? 'Enter a password 6+ chars long'
                            : null,
                        controller: _password,
                      ),
                    ),
                    SizedBox(height: size.height * 0.02),
                    customText('Repeat Password'),
                    Container(
                      height: size.height * 0.05,
                      child: TextFormField(
                          obscureText: true,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.lock),
                            contentPadding: EdgeInsets.fromLTRB(15, 20, 10, 0),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0))),
                          ),
                          validator: (val) => val != _password.text
                              ? 'Repeat password are not the same'
                              : null,
                          controller: _confirmPass),
                    ),
                    SizedBox(height: size.height * 0.02),
                    Center(
                      child: OutlinedButton(
                        child: Text(
                          'Next',
                          style:
                              TextStyle(fontSize: 15, color: Colors.blue[900]),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            // tos dialog navigates to create Account Info Page
                            // after user agrees to tos
                            tosAgreement(context, _email.text, _password.text,
                                widget._instructor);
                          }
                        },
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.lightBlue[50])),
                      ),
                    ),
                    SizedBox(height: 12.0),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
