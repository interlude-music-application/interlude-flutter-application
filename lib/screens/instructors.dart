import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';

import 'bottom_nav.dart';
import 'chat_room.dart';
import 'main_drawer.dart';

class SearchInstructors extends StatefulWidget {
  @override
  _SearchInstructors createState() => _SearchInstructors();
}

class _SearchInstructors extends State<SearchInstructors> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController searchTextEditingController =
      new TextEditingController();
  TextEditingController searchTextEditingControllerInstrum =
      new TextEditingController();

  String _state = "Any";
  QuerySnapshot searchSnapshot;

  List<String> statesOptional = [
    'Any',
    'AK',
    'AL',
    'AR',
    'AZ',
    'CA',
    'CO',
    'CT',
    'DE',
    'FL',
    'GA',
    'HI',
    'IA',
    'ID',
    'IL',
    'IN',
    'KS',
    'KY',
    'LA',
    'MA',
    'MD',
    'ME',
    'MI',
    'MN',
    'MO',
    'MS',
    'MT',
    'NC',
    'ND',
    'NE',
    'NH',
    'NJ',
    'NM',
    'NV',
    'NY',
    'OH',
    'OK',
    'OR',
    'PA',
    'RI',
    'SC',
    'SD',
    'TN',
    'TX',
    'UT',
    'VA',
    'VT',
    'WA',
    'WI',
    'WV',
    'WY'
  ];

  initiateSearch(String state, String gradeStr, String instrumentStr) {
    databaseMethods
        .getFilteredInstructors(state, gradeStr, instrumentStr)
        .then((val) {
      if (mounted) {
        setState(() {
          searchSnapshot = val;
        });
      }
    });
  }

  addInstrument() {}

  Widget searchList() {
    return searchSnapshot != null && searchSnapshot.docs.length != 0
        ? Expanded(
            child: ListView.separated(
            physics: BouncingScrollPhysics(),
            itemCount: searchSnapshot.docs.length,
            itemBuilder: (content, index) {
              Map<String, dynamic> data = searchSnapshot.docs[index].data();
              // print(data);
              return searchTile(
                  userName: data["name"],
                  userState: data["state"],
                  userGrade: data["grade_level"],
                  userInstruments: data["instruments"],
                  userEmail: data["email"],
                  userAvailability: data["availability"],
                  userBio: (data.containsKey("bio")) ? data["bio"] : "");
            },
            separatorBuilder: (BuildContext context, int index) => Divider(
              color: Theme.of(context).scaffoldBackgroundColor,
              height: 5,
            ),
          ))
        : Center(
            child: Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Text("No Available Instructors",
                    style: TextStyle(
                        fontFamily: 'Monsterrat',
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold))));
  }

  Widget searchTile(
      {String userName,
      String userState,
      String userGrade,
      String userInstruments,
      String userEmail,
      String userAvailability,
      String userBio}) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Row(children: [
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Row(children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 7),
                          child: Text(userName + " - " + userInstruments,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                        ),
                        Text(userState + ", Grade " + userGrade),
                        Text(""),
                        Text(""),
                        (userAvailability != '')
                            ? Text("Availability:\n" +
                                userAvailability.replaceAll("/n", "\n") +
                                "\n")
                            : Container()
                      ]),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ChatRoom(userName, userEmail)));
                    },
                    child: Container(
                      width: 90.0,
                      decoration: BoxDecoration(
                          color: Colors.blue[700],
                          borderRadius: BorderRadius.circular(30)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      child: Text(
                        "Message",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ]),
                Text(userBio),
              ])),
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Search Instructors', true),
      drawer: MainDrawer(),
      body: Container(
          child: Column(children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
          child: Row(children: [
            Expanded(
                child: Card(
                    child: ExpansionTile(
              initiallyExpanded: true,
              title: Text(
                "Filter Instructors",
                style: TextStyle(fontSize: 16.0),
              ),
              children: <Widget>[
                ListTile(
                    title: Row(children: <Widget>[
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('State'),
                        ButtonTheme(
                            child: DropdownButton<String>(
                                value: _state,
                                icon: const Icon(Icons.expand_more),
                                isDense: true,
                                iconSize: 24,
                                elevation: 16,
                                underline: Container(
                                  height: 0.5,
                                  color: Colors.black,
                                ),
                                onChanged: (String newValue) {
                                  if (mounted) {
                                    setState(() {
                                      _state = newValue;
                                    });
                                  }
                                },
                                items: statesOptional
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList())),
                      ]),
                  SizedBox(
                    width: 25.0,
                  ),
                  Container(
                    width: 45,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Grade'),
                          TextField(
                            decoration: InputDecoration(
                              isDense: true,
                              contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                            ),
                            controller: searchTextEditingController,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly
                            ],
                          ),
                        ]),
                  ),
                  SizedBox(
                    width: 25.0,
                  ),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                        Text('Instrument(s)'),
                        TextField(
                          decoration: InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                          ),
                          controller: searchTextEditingControllerInstrum,
                          keyboardType: TextInputType.name,
                          inputFormatters: <TextInputFormatter>[
                            //FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ])),
                  Tooltip(
                      message: "Use , to separate instruments",
                      verticalOffset: 25,
                      child: Icon(Icons.error_outlined,
                          color: Colors.blue, size: 35))
                ])),
                GestureDetector(
                  onTap: () {
                    initiateSearch(_state, searchTextEditingController.text,
                        searchTextEditingControllerInstrum.text);
                  },
                  child: ListTile(
                    title: Text(
                      "GO",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.blue),
                    ),
                  ),
                ),
              ],
            ))),
          ]),
        ),
        searchList(),
      ])),
      bottomNavigationBar: BottomNav(0, true),
    );
  }
}
