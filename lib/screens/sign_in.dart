import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/authenticate/auth.dart';
import 'package:interlude_music_app/constants.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/screens/sign_up.dart';
import 'package:interlude_music_app/ui/sharedWidgets/customText.dart';

import 'forgot_password.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  DatabaseMethods databaseMethods = new DatabaseMethods();
  QuerySnapshot snapshotUserInfo;

  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(children: <Widget>[
          Container(
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/conductor.jpg'),
                  fit: BoxFit.cover,
                  colorFilter:
                      ColorFilter.mode(Colors.white, BlendMode.multiply)),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 1,
            heightFactor: 0.4,
            child: FractionallySizedBox(
              widthFactor: 0.7,
              heightFactor: 0.3,
              child: Image(
                image: AssetImage('assets/interludetitlewhite.png'),
              ),
            ),
          ),
          Positioned(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                  width: double.infinity,
                  padding:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24),
                      topRight: Radius.circular(24),
                    ),
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: size.height * 0.02),
                        Center(
                          child: Text(
                            'Login',
                            style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: Colors.black,
                            ),
                          ),
                        ),
                        SizedBox(height: size.height * 0.05),
                        customText('Email'),
                        Container(
                          height: 40,
                          child: TextFormField(
                            controller: _email,
                            decoration: InputDecoration(
                              suffixIcon: Icon(Icons.person),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 15.0),
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                              ),
                            ),
                            validator: (val) =>
                                val.isEmpty ? 'Enter an email' : null,
                          ),
                        ),
                        SizedBox(height: size.height * 0.04),
                        customText('Password'),
                        Container(
                          height: 40,
                          child: TextFormField(
                              controller: _password,
                              obscureText: true,
                              decoration: InputDecoration(
                                suffixIcon: Icon(Icons.lock),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 15.0),
                                border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20.0)),
                                  gapPadding: 20.0,
                                ),
                              ),
                              validator: (val) => val.length < 6
                                  ? 'Enter a password 6+ chars long'
                                  : null),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ForgotPassword()));
                          },
                          child: Container(
                              padding: EdgeInsets.fromLTRB(25, 10, 0, 0),
                              child: Text(
                                'Forgot Password?',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.w300),
                              )),
                        ),
                        SizedBox(height: size.height * 0.04),
                        Center(
                          child: Container(
                            height: 35,
                            width: 100,
                            child: SizedBox(
                              width: double.infinity,
                              child: OutlinedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.lightBlue[50])),
                                child: Text('Login',
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.blue[900])),
                                onPressed: () async {
                                  // save email pref before verifying sign in because
                                  // Authcheck() redirects to Home() before onPressed
                                  // can finish and shared preferences can save
                                  await HelperFunctions
                                      .saveUserEmailSharedPreference(
                                          _email.text);

                                  dynamic result =
                                      await _auth.signInWithEmailAndPassword(
                                          _email.text, _password.text);
                                  if (result == null) {
                                    if (mounted) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                        content: Text(
                                          "Invalid Credentials",
                                        ),
                                      ));
                                    }
                                  } else {
                                    // print(_email.text);
                                    await HelperFunctions
                                        .saveUserLoggedInSharedPreference(true);
                                    await databaseMethods
                                        .getUserByUserEmail(_email.text)
                                        .then((val) {
                                      snapshotUserInfo = val;
                                      Map<String, dynamic> data =
                                          snapshotUserInfo.docs.isEmpty
                                              ? Map()
                                              : snapshotUserInfo.docs[0].data();

                                      HelperFunctions
                                          .saveUserNameSharedPreference(
                                              data["name"]);

                                      Constants.myEmail = _email.text;
                                      Constants.myName = data["name"];

                                      HelperFunctions
                                          .saveUserIsInstructorSharedPreference(
                                              data["instructor"] ?? false);
                                    });
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: size.height * 0.02),
                        Center(
                          child: TextButton(
                            child: Text("Don't have an account? Sign Up",
                                style: TextStyle(
                                  fontFamily: 'Monsterrat',
                                  fontSize: 12,
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                )),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SignUp(),
                                      fullscreenDialog: true));
                            },
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom)),
                      ],
                    ),
                  )),
            ),
          )
        ]),
      ),
    );
  }
}
