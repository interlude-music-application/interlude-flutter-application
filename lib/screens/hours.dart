import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';

import 'bottom_nav.dart';
import 'main_drawer.dart';

class Hours extends StatefulWidget {
  @override
  _HoursState createState() => _HoursState();
}

class _HoursState extends State<Hours> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController searchTextEditingController =
      new TextEditingController();

  String _instructorID;
  List _names = List.empty(growable: true);
  List _emails;
  Map hours;

  getStudents() {
    databaseMethods.getUserByUserEmail(_instructorID).then((val) {
      if (mounted) {
        setState(() {
          if (val.docs[0].data()["hours"] != null) {
            hours = val.docs[0].data()["hours"];
            _emails = hours.keys.toList();
          }
        });
      }
      if (hours != null) {
        for (var i = 0; i < hours.length; i++) {
          getStudentName(_emails[i]);
        }
      }
    });
  }

  getStudentName(String _studentID) {
    databaseMethods.getUserByUserEmail(_studentID).then((val) {
      if (mounted) {
        setState(() {
          _names.add(val.docs[0].data()["name"]);
        });
      }
    });
  }

  Widget hoursList() {
    return hours != null && hours.keys.length != 0 && _names.length != 0
        ? ListView.builder(
            itemCount: hours.length,
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (content, index) {
              return StudentHoursTile(
                userName: _names[index],
                hours: hours[_emails[index]],
              );
            },
          )
        : Center(
            child: Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Text("You have no hours yet",
                    style: TextStyle(
                        fontFamily: 'Monsterrat',
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Hours', true),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [hoursList()],
        ),
      ),
      bottomNavigationBar: BottomNav(0, true),
    );
  }

  @override
  void initState() {
    getUserID();
    super.initState();
  }

  void getUserID() async {
    String id = await HelperFunctions.getUserEmailSharedPreference();
    if (mounted) {
      setState(() {
        _instructorID = id;
        getStudents();
      });
    }
  }
}

class StudentHoursTile extends StatelessWidget {
  final String userName;
  final double hours;

  StudentHoursTile({this.userName, this.hours});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        userName != null ? userName : 'Username',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(hours != null ? hours.toStringAsFixed(1) : '0',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 24))),
            ),
          ],
        ),
        Divider(
          color: Colors.black26,
          thickness: 1,
        )
      ],
    );
  }
}
