import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/screens/edit_profile.dart';

class EditBio extends StatefulWidget {
  @override
  _EditBio createState() => _EditBio();

  final String _docID;
  final bool _isInstructor;
  var data;

  EditBio(this._docID, this._isInstructor, this.data);
}

class _EditBio extends State<EditBio> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  //TextEditingController _bio = TextEditingController(text: widget.data);

  @override
  Widget build(BuildContext context) {
    TextEditingController _bio =
        TextEditingController(text: widget.data["bio"]);

    void _submit() async {
      Map<String, Object> userMap = new Map();

      if (_bio.text.isNotEmpty) {
        userMap['bio'] = _bio.text;
        widget.data["bio"] = _bio.text;
      }

      if (userMap.isNotEmpty) {
        await databaseMethods.updateUserInfo(widget._docID, userMap);
      }

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EditProfile(
                  widget._docID, widget._isInstructor, widget.data)));
    }

    Widget textCard = Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 30),
      child: Text(
        'Edit Bio',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: 'Montserrat',
          fontSize: 30,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
      ),
    );

    Widget textEditorCard = Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
        ),
        maxLines: null,
        textAlign: TextAlign.left,
        controller: _bio,
        keyboardType: TextInputType.multiline,
      ),
    );

    Widget buttonCard = OutlinedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color>(Colors.lightBlue[50])),
        onPressed: () async {
          _submit();
        },
        child: Text(
          'Update',
          style: TextStyle(fontSize: 15, color: Colors.blue[900]),
        ));

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: primaryBar('Edit Bio', true),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
              ),
              textCard,
              textEditorCard,
              SizedBox(height: 30.0),
              buttonCard,
              Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom)),
            ],
          ),
        ));
  }
}
