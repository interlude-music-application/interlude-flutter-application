import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/screens/sign_in.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'create_acct.dart';

class VerifyInstructor extends StatefulWidget {
  @override
  _VerifyInstructorState createState() => _VerifyInstructorState();
}

class _VerifyInstructorState extends State<VerifyInstructor> {
  StreamController<ErrorAnimationType> errorController;
  final _instructorPin = TextEditingController(); // make this number input only
  String currentText = "";
  DatabaseMethods databaseMethods = new DatabaseMethods();
  QuerySnapshot snapshotUserInfo;

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(children: <Widget>[
          Container(
            height: size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/conductor.jpg'),
                  fit: BoxFit.cover,
                  colorFilter:
                      ColorFilter.mode(Colors.white, BlendMode.multiply)),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 1,
            heightFactor: 0.4,
            child: FractionallySizedBox(
              widthFactor: 0.7,
              heightFactor: 0.3,
              child: Image(
                image: AssetImage('assets/interludetitlewhite.png'),
              ),
            ),
          ),
          Positioned(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(24),
                    topRight: Radius.circular(24),
                  ),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: size.height * 0.02),
                    Center(
                      child: Text(
                        'Instructor Verification',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w500,
                          fontSize: 26,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(height: size.height * 0.02),
                    Center(
                      child: Text(
                        'Enter the Instructor Verification Pin',
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(height: size.height * 0.05),
                    PinCodeTextField(
                      appContext: context,
                      length: 6,
                      obscureText: false,
                      keyboardType: TextInputType.number,
                      animationType: AnimationType.fade,
                      animationDuration: Duration(milliseconds: 300),
                      errorAnimationController: errorController,
                      controller: _instructorPin,
                      onCompleted: (v) {
                        // print("Completed");
                      },
                      onChanged: (value) {
                        // print(value);
                        setState(() {
                          currentText = value;
                        });
                      },
                      beforeTextPaste: (text) {
                        // print("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return true;
                      },
                    ),
                    SizedBox(height: size.height * 0.04),
                    Center(
                      child: Container(
                        height: 35,
                        width: 100,
                        child: SizedBox(
                          width: double.infinity,
                          child: OutlinedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Colors.lightBlue[50])),
                            child: Text('Verify',
                                style: TextStyle(
                                    fontSize: 15, color: Colors.blue[900])),
                            onPressed: () async {
                              try {
                                int.parse(currentText);
                              } catch (error) {
                                return;
                              }

                              databaseMethods
                                  .getInstructorVerificationPin()
                                  .then((val) {
                                // print(val.data()["pin"]);
                                if (int.parse(currentText) !=
                                    val.data()["pin"]) {
                                  errorController.add(ErrorAnimationType.shake);
                                } else {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CreateAccount(true)));
                                }
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: size.height * 0.02),
                    Center(
                      child: TextButton(
                        child: Text("Already have an account? Login",
                            style: TextStyle(
                              fontFamily: 'Monsterrat',
                              fontSize: 12,
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                            )),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignIn(),
                                  fullscreenDialog: true));
                        },
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom)),
                  ],
                ),
              ),
            ),
          )
        ]),
      ),
    );
  }
}
