import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';

import 'bottom_nav.dart';
import 'main_drawer.dart';

class QA {
  String q; //Question Variable
  String a; //Answer Variable

  QA({this.q, this.a});
}

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  final _generalQAList = _buildGeneralQAList();
  final _studentQAList = _buildStudentQAList();
  final _instructorQAList = _buildInstructorQAList();

  final _QText = TextStyle(
    fontFamily: 'Monsterrat',
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );

  final _AText = TextStyle(
    fontFamily: 'Monsterrat',
    color: Colors.black,
    fontSize: 16,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Help', true),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Card(
                child: ExpansionTile(
                  title: Text(
                    "General FAQ's",
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  children: <Widget>[
                    ListView.separated(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: _generalQAList.length,
                      itemBuilder: (context, index) {
                        return Container(
                          color: Color.fromRGBO(172, 173, 188, 0.11),
                          child: ListTile(
                            title: Text('Q: ' + _generalQAList[index].q,
                                style: _QText),
                            subtitle: Text('A: ' + _generalQAList[index].a,
                                style: _AText),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        height: 5,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              child: Card(
                child: ExpansionTile(
                  title: Text(
                    "Student FAQ's",
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  children: <Widget>[
                    ListView.separated(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: _studentQAList.length,
                      itemBuilder: (context, index) {
                        return Container(
                          color: Color.fromRGBO(172, 173, 188, 0.11),
                          child: ListTile(
                            title: Text('Q: ' + _studentQAList[index].q,
                                style: _QText),
                            subtitle: Text('A: ' + _studentQAList[index].a,
                                style: _AText),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        height: 5,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Flexible(
              child: Card(
                child: ExpansionTile(
                  title: Text(
                    "Instructor FAQ's",
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                  ),
                  children: <Widget>[
                    ListView.separated(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      itemCount: _instructorQAList.length,
                      itemBuilder: (context, index) {
                        return Container(
                          color: Color.fromRGBO(172, 173, 188, 0.11),
                          child: ListTile(
                            title: Text('Q: ' + _instructorQAList[index].q,
                                style: _QText),
                            subtitle: Text('A: ' + _instructorQAList[index].a,
                                style: _AText),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          Divider(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        height: 5,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNav(0, true),
    );
  }
}

List<QA> _buildGeneralQAList() {
  List<QA> _qaList = [];

  _qaList.add(new QA(
      q: 'How can I access my profile?',
      a: 'To visit your profile, tap on your name in the top left of the sidebar.'));
  _qaList.add(new QA(
      q: 'How do I edit my profile information?',
      a: 'Next to your name on your Profile page, tap the pencil icon to edit your profile information.'));
  _qaList.add(new QA(
      q: 'Can I change my email address?',
      a: 'No. You cannot change your email address after creating an account.'));
  _qaList.add(new QA(
      q: 'How can I change my password?',
      a: 'Under the edit profile page, tap the change password button. This will send a password reset link to your email.'));
  _qaList.add(new QA(
      q: 'How can I view my lesson history?',
      a: 'Under your profile page, there is your lesson history. Press the new window icon for better viewing.'));
  _qaList.add(new QA(
      q: 'How can I message someone?',
      a: 'Under the Chats page, search for a user by their name. Once you find the person you are looking for, you can start chatting.'));
  _qaList.add(new QA(
      q: 'How can I view the time that a message was sent?',
      a: 'Tap a message bubble to toggle the time stamp visibility.'));
  _qaList.add(new QA(
      q: 'Why isn\'t my lesson status updating?',
      a: 'Lessons are completed periodically, so you may not see it complete right away.'));
  _qaList.add(new QA(
      q: 'Who can I contact for help with the app or anything else?',
      a: 'Contact US:\n'
          'interlude@interludemusic.org\n'
          'interludemusicaloutreach@gmail.com\n\n'
          'For more information, visit: \n'
          'https://www.interludeoutreach.org/\n'
          'https://www.linkedin.com/company/interlude-musical-outreach\n'));

  return _qaList;
}

List<QA> _buildStudentQAList() {
  List<QA> _qaList = [];

  _qaList.add(new QA(
      q: 'How do I find an instructor?',
      a: 'From the sidebar, visit the Instructors page. Here you can search all available instructors. Once you find a good match, you can message them to set up lessons.'));
  _qaList.add(new QA(
      q: 'How do I schedule a lesson?',
      a: 'Students cannot schedule lessons. Instead, message your instructor and they will schedule one for you. Once they do this, you can see it under the Lessons page.'));
  _qaList.add(new QA(
      q: 'How do the prizes work?',
      a: 'By taking more lessons and earning more hours, you can unlock higher level prizes.'));

  return _qaList;
}

List<QA> _buildInstructorQAList() {
  List<QA> _qaList = [];

  _qaList.add(new QA(
      q: 'How do I schedule/cancel a lesson?',
      a: 'Navigate to the Lessons page. Here you can add lessons by tapping the plus sign and cancel lessons by tapping the trash can.'));
  _qaList.add(new QA(
      q: 'How can I set my availability?',
      a: 'Under the edit profile page (tap name in sidebar > tap pencil icon) press the availability button. Here you can add another availability range or overwrite your current availability.'));
  _qaList.add(new QA(
      q: 'How can I set/edit my bio (description visible to students)?',
      a: 'Under the edit profile page (tap name in sidebar > tap pencil icon) press the edit bio button. Here you can edit your bio. This is what students will see when looking for an instructor.'));
  _qaList.add(new QA(
      q: 'How can I stop/resume accepting new students?',
      a: 'Under the edit profile page (tap name in sidebar > tap pencil icon) you can set whether you are currently accepting new students or not.'));

  return _qaList;
}
