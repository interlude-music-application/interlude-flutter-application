import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:intl/intl.dart';

import 'bottom_nav.dart';
import 'edit_profile.dart';
import 'lesson_history.dart';
import 'main_drawer.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final TextStyle _subtext =
      TextStyle(fontFamily: 'Monsterrat', color: Colors.black, fontSize: 14);

  final DatabaseMethods databaseMethods = new DatabaseMethods();

  //This is the user's email as of now; subject to change if needed.
  String _profileID;
  bool _isStudent = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('My Profile', true),
      drawer: MainDrawer(),
      body: FutureBuilder<List<dynamic>>(
        future: Future.wait(
          [
            databaseMethods.getUserByUserEmail(_profileID),
            databaseMethods.getLessonsUser(_profileID, _isStudent, true, false)
          ],
        ),
        builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
          if (snapshot.hasError) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                "Oops! It looks like an error has occurred...",
              ),
            ));
            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data = snapshot.data[0].docs.isEmpty
                ? Map()
                : snapshot.data[0].docs.first.data();
            List lessonList = snapshot.data[1].docs;
            int nextLesson = getNextLesson(lessonList);

            Widget infoCard = Container(
                margin: EdgeInsets.symmetric(vertical: 8),
                padding: EdgeInsets.all(15),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    !_isStudent
                        ? Icon(
                            Icons.school,
                            color: Colors.black,
                            size: 46.0,
                          )
                        : Icon(
                            Icons.backpack_outlined,
                            color: Colors.black,
                            size: 46.0,
                          ),
                    Expanded(
                        child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Flexible(
                                        fit: FlexFit.loose,
                                        child: Text(
                                          '${data['name'] ?? 'username'}',
                                          style: TextStyle(
                                            fontFamily: 'Monsterrat',
                                            color: Colors.black,
                                            fontSize: 20,
                                          ),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                        )),
                                    Flexible(
                                        fit: FlexFit.loose,
                                        child: Text(
                                          '- ${data['state']?.replaceAll("", "\u{200B}") ?? 'State'}',
                                          style: _subtext,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                        )),
                                  ],
                                ),
                                Text(
                                  '${data['instruments'] ?? 'No Instruments Specified'}',
                                  style: _subtext,
                                ),
                                Text(
                                  '${data['years_of_experience'] ?? 'Unknown'} Years of Experience',
                                  style: _subtext,
                                )
                              ],
                            ),
                          ),
                          !_isStudent
                              ? Container(
                                  padding: EdgeInsets.symmetric(vertical: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Availability:',
                                        style: _subtext,
                                      ),
                                      Text(
                                          (data['availability'] != null)
                                              ? data['availability']
                                                  .replaceAll("/n", "\n")
                                              : '',
                                          style: _subtext),
                                    ],
                                  ),
                                )
                              : Container(),
                          // Rating feature not implemented
                          // Maybe implement later and reuse this code
                          // !_isStudent
                          //     ? SmoothStarRating(
                          //         allowHalfRating: true,
                          //         isReadOnly: true,
                          //         starCount: 5,
                          //         rating: data['rating'] ?? 5,
                          //         color: Color.fromRGBO(194, 188, 41, 1),
                          //         borderColor: Color.fromRGBO(194, 188, 41, 1),
                          //       )
                          //     : Container()
                        ],
                      ),
                    )),
                    IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: Colors.black,
                          size: 28.0,
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditProfile(
                                      snapshot.data[0].docs.first.id,
                                      !_isStudent,
                                      data)));
                        }),
                  ],
                ));

            Widget hourCard = Expanded(
                child: Container(
                    margin: EdgeInsets.only(right: 4),
                    padding: EdgeInsets.all(15),
                    color: Colors.white,
                    child: Column(
                      children: [
                        Text(
                          '${data['total_hours'].toInt() ?? '0'}',
                          style: TextStyle(
                              fontFamily: 'Monsterrat',
                              color: Colors.black,
                              fontSize: 38),
                        ),
                        Text(
                          (!_isStudent) ? 'Hours Taught' : 'Hours of Learning',
                          style: _subtext,
                        )
                      ],
                    )));

            Widget nextLessonCard = Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 4),
                    padding: EdgeInsets.all(15),
                    color: Colors.white,
                    child: nextLesson != -1
                        ? Column(
                            children: [
                              Text(
                                '${DateFormat.Md().format(lessonList[nextLesson].data()["startTime"].toDate())}',
                                style: TextStyle(
                                    fontFamily: 'Monsterrat',
                                    color: Colors.black,
                                    fontSize: 30),
                              ),
                              Text(
                                '${DateFormat.jm().format(lessonList[nextLesson].data()["startTime"].toDate())}-${DateFormat.jm().format(lessonList[nextLesson].data()["endTime"].toDate())} ${lessonList[nextLesson].data()["startTime"].toDate().timeZoneName}',
                                style: TextStyle(
                                    fontFamily: 'Monsterrat',
                                    color: Colors.black,
                                    fontSize: 10),
                              ),
                              Text(
                                'Your Next Lesson',
                                style: _subtext,
                              )
                            ],
                          )
                        : Text(
                            'No upcoming lessons are currently scheduled',
                            style: TextStyle(
                                fontFamily: 'Monsterrat',
                                color: Colors.black,
                                fontSize: 15),
                            textAlign: TextAlign.center,
                          )));

            Widget lessonListCard = Expanded(
              child: ListView.separated(
                  physics: BouncingScrollPhysics(),
                  itemCount: lessonList.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(15),
                      color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '${DateFormat.yMMMd().format(lessonList[index].data()["startTime"].toDate())}',
                                style: TextStyle(
                                    fontFamily: 'Monsterrat',
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              //It would be nice to make this cleaner but dateformat can't handle timezones
                              Text(
                                  '${DateFormat.jm().format(lessonList[index].data()["startTime"].toDate())}-${DateFormat.jm().format(lessonList[index].data()["endTime"].toDate())} ${lessonList[index].data()["startTime"].toDate().timeZoneName}',
                                  style: TextStyle(
                                      fontFamily: 'Monsterrat',
                                      color: Colors.black,
                                      fontSize: 14))
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                _isStudent
                                    ? '${lessonList[index].data()["instructor_name"]}'
                                    : '${lessonList[index].data()["student_name"]}',
                                style: TextStyle(
                                    fontFamily: 'Monsterrat',
                                    color: getColor(
                                        lessonList[index].data()["status"]),
                                    fontSize: 15),
                              ),
                              Text(
                                '${lessonList[index].data()["status"]}',
                                style: TextStyle(
                                    fontFamily: 'Monsterrat',
                                    color: getColor(
                                        lessonList[index].data()["status"]),
                                    fontSize: 15),
                              )
                            ],
                          )
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        height: 2,
                      )),
            );

            Widget lessonHistoryCard = Container(
              margin: EdgeInsets.only(top: 8, bottom: 2),
              padding: EdgeInsets.all(15),
              color: Colors.white,
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Text(
                      'Lesson History',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Monsterrat',
                          color: Colors.black,
                          fontSize: 20),
                    ),
                  ),
                  Container(
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LessonHistory()));
                        },
                        child: Icon(
                          Icons.launch,
                          color: Colors.black,
                          size: 24.0,
                        ),
                      ))
                ],
              ),
            );

            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                infoCard,
                IntrinsicHeight(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [hourCard, nextLessonCard],
                  ),
                ),
                lessonHistoryCard,
                lessonListCard,
              ],
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      bottomNavigationBar: BottomNav(0, true),
    );
  }

  @override
  void initState() {
    getProfileId();
    isStudent();
    super.initState();
  }

  void getProfileId() async {
    String id = await HelperFunctions.getUserEmailSharedPreference();

    if (mounted) {
      setState(() {
        _profileID = id;
      });
    }
  }

  void isStudent() async {
    bool isInstructor =
        await HelperFunctions.getUserIsInstructorSharedPreference();
    if (mounted) {
      setState(() {
        _isStudent = !isInstructor;
      });
    }
  }
}

int getNextLesson(list) {
  DateTime now = DateTime.now();
  int index = -1;

  for (var i = 0; i < list.length; i++) {
    if (now.isBefore(list[i].data()["startTime"].toDate())) {
      if (list[i].data()["status"] == "Upcoming") {
        index = i;
      }
    } else {
      break;
    }
  }

  return index;
}
