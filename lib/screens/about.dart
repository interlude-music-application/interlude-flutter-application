import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';

import 'bottom_nav.dart';
import 'main_drawer.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('About Us', true),
      drawer: MainDrawer(),
      body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    child: SelectableText(
                        'Interlude Musical Outreach is a PVSA Certified '
                        'Organization, founded in 2016, that aims to provide '
                        'instrumental music education and opportunities for all '
                        'aspiring students. Its goal is to inspire and support young '
                        'musicians with free private lessons, workshops, and '
                        'opportunities to perform throughout the community.\n\n'),
                  ),
                  Center(
                    child: Image(
                      image: AssetImage('assets/interludeImgGrid.png'),
                    ),
                  ),
                  Container(
                    child: SelectableText(
                        '\n\nIn light of COVID-19, Interlude has begun the '
                        'MUSICfromHOME initiative, an online private lesson service open '
                        'to all elementary and middle school students. Students are '
                        'matched with experienced and qualified high school '
                        'musicians and have weekly private lessons. \n\n'
                        'Interlude Musical Outreach also offers free workshops on topics '
                        'such as music theory, honor orchestra auditions '
                        '(All-Southern, All-State, All-National Orchestras and more)!\n\n'
                        'Contact:\n'
                        'interlude@interludemusic.org\n'
                        'interludemusicaloutreach@gmail.com\n\n'
                        'For more information, visit: \n'
                        'https://www.interludeoutreach.org/\n'
                        'https://www.linkedin.com/company/interlude-musical-outreach\n'),
                  ),

                  // TODO: Add Icon Buttons with links instead of pasting these links
                  // see package: https://pub.dev/packages/url_launcher
                  // see stack overflow page: https://stackoverflow.com/questions/43149055/how-do-i-open-a-web-browser-url-from-my-flutter-code
                  // Container(
                  //   child: Row(
                  //     children: [
                  //       IconButton(
                  //           icon: Icon(
                  //             Icons.mail,
                  //           ),
                  //           onPressed: () async {
                  //             const url = 'mailto:interlude@interludemusic.org';
                  //             // if (await canLaunch(url)) {
                  //             //   await launch(url);
                  //             // } else {
                  //             //   throw 'Could not launch $url';
                  //             // }
                  //           })
                  //     ],
                  //   ),
                  // )
                ],
              ))),
      bottomNavigationBar: BottomNav(0, true),
    );
  }
}
