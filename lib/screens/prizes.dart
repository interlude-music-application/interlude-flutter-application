import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import 'bottom_nav.dart';
import 'main_drawer.dart';

class Prizes extends StatefulWidget {
  @override
  _PrizesState createState() => _PrizesState();

  final double hours;
  Prizes({this.hours});
}

class _PrizesState extends State<Prizes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Prizes', true),
      drawer: MainDrawer(),
      body: MyPrizes(
        hours: widget.hours,
      ),
      bottomNavigationBar: BottomNav(0, true),
    );
  }
}

class MyPrizes extends StatelessWidget {
  final double hours;

  MyPrizes({this.hours});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: SafeArea(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    // print('1 Hour pressed');
                  },
                  child: Card(
                    margin: EdgeInsets.only(top: 20.0),
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                          border: hours >= 1
                              ? Border.all(
                                  color: Colors.blue[800],
                                  width: 0.5,
                                )
                              : null,
                          borderRadius: BorderRadius.circular(8)),
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: hours >= 1
                                          ? Colors.blue[800]
                                          : Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      '1 Hour',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          hours > 0 && hours < 1
                              ? LinearPercentIndicator(
                                  animation: true,
                                  lineHeight: 6,
                                  percent: hours / 1,
                                  linearStrokeCap: LinearStrokeCap.butt,
                                  progressColor: Colors.blue[800],
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  backgroundColor: Colors.grey,
                                )
                              : Container(),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Center(
                            child: hours >= 1
                                ? Image(
                                    image: AssetImage(
                                        'assets/colored_music_note.png'),
                                  )
                                : Image(
                                    image: AssetImage(
                                        'assets/gray_music_note.png'),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // print('5 Hour pressed');
                  },
                  child: Card(
                    margin: EdgeInsets.only(top: 20.0),
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                          border: hours >= 5
                              ? Border.all(
                                  color: Colors.blue[800],
                                  width: 0.5,
                                )
                              : null,
                          borderRadius: BorderRadius.circular(8)),
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: hours >= 5
                                          ? Colors.blue[800]
                                          : Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      '5 Hours',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          hours > 1 && hours < 5
                              ? LinearPercentIndicator(
                                  animation: true,
                                  lineHeight: 6,
                                  percent: hours / 5,
                                  linearStrokeCap: LinearStrokeCap.butt,
                                  progressColor: Colors.blue[800],
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  backgroundColor: Colors.grey,
                                )
                              : Container(),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Center(
                            child: hours >= 5
                                ? Image(
                                    image: AssetImage(
                                        'assets/colored_scale_bars.png'),
                                  )
                                : Image(
                                    image: AssetImage(
                                        'assets/gray_scale_bars.png'),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    // print('10 Hour pressed');
                  },
                  child: Card(
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                          border: hours >= 10
                              ? Border.all(
                                  color: Colors.blue[800],
                                  width: 0.5,
                                )
                              : null,
                          borderRadius: BorderRadius.circular(8)),
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: hours >= 10
                                          ? Colors.blue[800]
                                          : Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      '10 Hours',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          hours > 5 && hours < 10
                              ? LinearPercentIndicator(
                                  animation: true,
                                  lineHeight: 6,
                                  percent: hours / 10,
                                  linearStrokeCap: LinearStrokeCap.butt,
                                  progressColor: Colors.blue[800],
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  backgroundColor: Colors.grey,
                                )
                              : Container(),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Center(
                            child: hours >= 10
                                ? Image(
                                    image:
                                        AssetImage('assets/colored_trophy.png'),
                                  )
                                : Image(
                                    image: AssetImage('assets/gray_trophy.png'),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // print('20 Hour pressed');
                  },
                  child: Card(
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                          border: hours >= 20
                              ? Border.all(
                                  color: Colors.blue[800],
                                  width: 0.5,
                                )
                              : null,
                          borderRadius: BorderRadius.circular(8)),
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: hours >= 20
                                          ? Colors.blue[800]
                                          : Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      '20 Hours',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          hours > 10 && hours < 20
                              ? LinearPercentIndicator(
                                  animation: true,
                                  lineHeight: 6,
                                  percent: hours / 20,
                                  linearStrokeCap: LinearStrokeCap.butt,
                                  progressColor: Colors.blue[800],
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  backgroundColor: Colors.grey,
                                )
                              : Container(),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Center(
                            child: hours >= 20
                                ? Image(
                                    image: AssetImage(
                                        'assets/colored_mountain_pike.png'),
                                  )
                                : Image(
                                    image: AssetImage(
                                        'assets/gray_mountain_pike.png'),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    // print('40 Hour pressed');
                  },
                  child: Card(
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                          border: hours >= 40
                              ? Border.all(
                                  color: Colors.blue[800],
                                  width: 0.5,
                                )
                              : null,
                          borderRadius: BorderRadius.circular(8)),
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: hours >= 40
                                          ? Colors.blue[800]
                                          : Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      '40 Hours',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          hours > 20 && hours < 40
                              ? LinearPercentIndicator(
                                  animation: true,
                                  lineHeight: 6,
                                  percent: hours / 40,
                                  linearStrokeCap: LinearStrokeCap.butt,
                                  progressColor: Colors.blue[800],
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  backgroundColor: Colors.grey,
                                )
                              : Container(),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Center(
                            child: hours >= 40
                                ? Image(
                                    image:
                                        AssetImage('assets/colored_Award.png'),
                                  )
                                : Image(
                                    image: AssetImage('assets/gray_Award.png'),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // print('60 Hour pressed');
                  },
                  child: Card(
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            border: hours >= 60
                                ? Border.all(
                                    color: Colors.blue[800], width: 0.5)
                                : null,
                            borderRadius: BorderRadius.circular(8)),
                        height: 150,
                        width: 150,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: hours >= 60
                                            ? Colors.blue[800]
                                            : Colors.grey,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            topRight: Radius.circular(8))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        '60 Hours',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            hours > 40 && hours < 60
                                ? LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 6,
                                    percent: hours / 60,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.blue[800],
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    backgroundColor: Colors.grey,
                                  )
                                : Container(),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),
                            Center(
                              child: hours >= 60
                                  ? Image(
                                      image: AssetImage(
                                          'assets/colored_rocket.png'),
                                    )
                                  : Image(
                                      image:
                                          AssetImage('assets/gray_rocket.png'),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    // print('100 Hour pressed');
                  },
                  child: Card(
                    margin: EdgeInsets.only(bottom: 20.0),
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            border: hours >= 100
                                ? Border.all(
                                    color: Colors.blue[800],
                                    width: 0.5,
                                  )
                                : null,
                            borderRadius: BorderRadius.circular(8)),
                        height: 150,
                        width: 150,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: hours >= 100
                                          ? Colors.blue[800]
                                          : Colors.grey,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      '100 Hours',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          hours > 60 && hours < 100
                              ? LinearPercentIndicator(
                                  animation: true,
                                  lineHeight: 6,
                                  percent: hours / 100,
                                  linearStrokeCap: LinearStrokeCap.butt,
                                  progressColor: Colors.blue[800],
                                  padding: EdgeInsets.symmetric(horizontal: 0),
                                  backgroundColor: Colors.grey,
                                )
                              : Container(),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Center(
                            child: hours >= 100
                                ? Image(
                                    image: AssetImage(
                                        'assets/colored_confetti.png'),
                                  )
                                : Image(
                                    image:
                                        AssetImage('assets/gray_confetti.png'),
                                  ),
                          ),
                        ]),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    // print('200 Hour pressed');
                  },
                  child: Card(
                    margin: EdgeInsets.only(bottom: 20.0),
                    elevation: 25.0,
                    shadowColor: Colors.black,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: SizedBox(
                      child: Container(
                        decoration: BoxDecoration(
                            border: hours >= 200
                                ? Border.all(
                                    color: Colors.blue[800],
                                    width: 0.5,
                                  )
                                : null,
                            borderRadius: BorderRadius.circular(8)),
                        height: 150,
                        width: 150,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: hours >= 200
                                            ? Colors.blue[800]
                                            : Colors.grey,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            topRight: Radius.circular(8))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        '200 Hours',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            100 < hours && hours < 200
                                ? LinearPercentIndicator(
                                    animation: true,
                                    lineHeight: 6,
                                    percent: hours / 200,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.blue[800],
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    backgroundColor: Colors.grey,
                                  )
                                : Container(),
                            Padding(
                              padding: EdgeInsets.all(8.0),
                            ),
                            Center(
                              child: hours >= 200
                                  ? Image(
                                      image: AssetImage(
                                          'assets/colored_crown.png'),
                                    )
                                  : Image(
                                      image:
                                          AssetImage('assets/gray_crown.png'),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
