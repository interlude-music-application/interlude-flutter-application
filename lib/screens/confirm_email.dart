import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/authenticate/auth.dart';

import 'home.dart';

class ConfirmEmail extends StatelessWidget {
  final AuthService _auth = new AuthService();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: primaryBar('Confirm Email', false),
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Text(
                  'A Confirmation Email has just been sent to you. ' +
                      'Click the link provided to complete your account setup.\n',
                  style: TextStyle(
                      fontFamily: 'Monsterrat',
                      color: Colors.black,
                      fontSize: 20),
                  textAlign: TextAlign.center,
                ),
                ElevatedButton(
                  child: Text('Send Again'),
                  onPressed: () async {
                    _auth.sendEmailVerification();
                  },
                ),
                Text(
                  '\nAfter this, click the \'OK\' button to return to your homepage.',
                  style: TextStyle(
                      fontFamily: 'Monsterrat',
                      color: Colors.black,
                      fontSize: 20),
                  textAlign: TextAlign.center,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            child: Text('OK'),
                            onPressed: () async {
                              if (FirebaseAuth.instance.currentUser != null) {
                                await FirebaseAuth.instance.currentUser
                                    .reload();
                                if (FirebaseAuth
                                    .instance.currentUser.emailVerified) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Home()));
                                }
                              }
                            },
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 10.0),
                          ),
                          ElevatedButton(
                            child: Text('Cancel'),
                            onPressed: () async {
                              await _auth.signOut();
                              Navigator.popUntil(
                                  context, ModalRoute.withName('/'));
                            },
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
