import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';

class Schedule extends StatefulWidget {
  final String _studentID;
  const Schedule(this._studentID);
  @override
  _Schedule createState() => _Schedule();
}

class _Schedule extends State<Schedule> {
  final DatabaseMethods databaseMethods = new DatabaseMethods();

  String _instructorID, _instructorName;
  DateTime _selectedDate = DateTime.now();
  TimeOfDay _startTime = TimeOfDay(hour: 12, minute: 0);
  TimeOfDay _endTime = TimeOfDay(hour: 13, minute: 0);

  Future<void> _selectTime(BuildContext context, bool start) async {
    TimeOfDay selected;
    if (start) {
      selected = await showTimePicker(
          context: context,
          initialTime: _startTime,
          initialEntryMode: TimePickerEntryMode.input);
      if (selected != null && selected != _startTime) {
        if (mounted) {
          setState(() {
            _startTime = selected;
          });
        }
      }
    } else {
      selected = await showTimePicker(
          context: context,
          initialTime: _endTime,
          initialEntryMode: TimePickerEntryMode.input);
      if (selected != null && selected != _endTime) {
        if (mounted) {
          setState(() {
            _endTime = selected;
          });
        }
      }
    }
  }

  void _submit(String sName) async {
    DateTime now = DateTime.now();
    DateTime start = DateTime(_selectedDate.year, _selectedDate.month,
            _selectedDate.day, _startTime.hour, _startTime.minute)
        .toUtc();
    DateTime end = DateTime(_selectedDate.year, _selectedDate.month,
            _selectedDate.day, _endTime.hour, _endTime.minute)
        .toUtc();

    //Throw error if times don't make sense
    //If this is causing errors, you are most likely on an old version of flutter.
    if (end.difference(start) <= Duration(minutes: 0)) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Starting time must be before ending time."),
      ));
      return null;
    }

    if (!now.isBefore(start)) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Starting time must be after current time."),
      ));
      return null;
    }

    databaseMethods.getUserByUserEmail(widget._studentID).then((val) {
      if (val.docs.length == 0) return;

      if (val.docs[0].data()["timezone"] != null &&
          val.docs[0].data()["tokens"] != null) {
        String studentTimezone = val.docs[0].data()["timezone"];
        List studentTokens = List.from(val.docs[0].data()["tokens"]);

        databaseMethods.getUserByUserEmail(_instructorID).then((val) {
          if (val.docs.length == 0) return;

          if (val.docs[0].data()["timezone"] != null &&
              val.docs[0].data()["tokens"] != null) {
            String instructorTimezone = val.docs[0].data()["timezone"];
            List instructorTokens = List.from(val.docs[0].data()["tokens"]);

            databaseMethods.addLesson({
              'instructor': _instructorID,
              'instructor_name': _instructorName,
              'instructor_timezone': instructorTimezone,
              'instructor_tokens': instructorTokens,
              'student': widget._studentID,
              'student_name': sName,
              'student_timezone': studentTimezone,
              'student_tokens': studentTokens,
              'duration': end.difference(start).inMinutes / 60,
              'endTime': end,
              'startTime': start,
              "status": 'Upcoming'
            });
          }
        });
      }
    });

    Navigator.pop(context);
  }

  void _cancel() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Schedule Lesson', false),
      backgroundColor: Colors.white,
      body: FutureBuilder(
        future: databaseMethods.getUserByUserEmail(widget._studentID),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                "Oops! It looks like an error has occurred...",
              ),
            ));
            return Container();
          }

          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> data = snapshot.data.docs.isEmpty
                ? Map()
                : snapshot.data.docs.first.data();

            Widget studentCard = Container(
              margin: EdgeInsets.only(top: 15),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                  Icon(Icons.school, color: Colors.black, size: 64),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 13),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${data['name'] ?? 'Student'}',
                          style: TextStyle(
                              fontFamily: 'Monsterrat',
                              color: Colors.black,
                              fontSize: 21),
                        ),
                        Text(
                          '${data['instruments'] ?? 'Instrument'}',
                          style: TextStyle(
                              fontFamily: 'Monsterrat',
                              color: Colors.black,
                              fontSize: 14),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3),
                  ),
                  Icon(Icons.schedule, color: Colors.black, size: 32),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                  )
                ],
              ),
            );

            Widget dateCard = Container(
                margin:
                    EdgeInsets.only(bottom: 0, top: 25, left: 50, right: 50),
                child: Column(children: [
                  Text(
                    'Date',
                    style: TextStyle(
                        fontFamily: 'Monsterrat',
                        color: Colors.black,
                        fontSize: 14),
                  ),
                  CalendarDatePicker(
                    initialDate: _selectedDate,
                    firstDate: DateTime.now(),
                    lastDate: _selectedDate.add(Duration(days: 365)),
                    onDateChanged: (DateTime value) {
                      _selectedDate = value;
                    },
                  )
                ]));

            Widget timeCard = Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Time',
                    style: TextStyle(
                        fontFamily: 'Monsterrat',
                        color: Colors.black,
                        fontSize: 14),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Flexible(
                          fit: FlexFit.tight,
                          child: TextButton(
                            child: Text(
                              _startTime.format(context),
                              style: TextStyle(
                                  fontFamily: 'Monsterrat', fontSize: 24),
                            ),
                            onPressed: () => _selectTime(context, true),
                          )),
                      Text('to',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: 'Monsterrat',
                              color: Colors.black,
                              fontSize: 14)),
                      Flexible(
                          fit: FlexFit.tight,
                          child: TextButton(
                            child: Text(_endTime.format(context),
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontFamily: 'Monsterrat', fontSize: 24)),
                            onPressed: () => _selectTime(context, false),
                          ))
                    ],
                  )
                ]);

            Widget buttonCard = Container(
                padding: EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    OutlinedButton(
                        child: Text('Schedule',
                            style: TextStyle(
                                fontFamily: 'Monsterrat',
                                color: Colors.black,
                                fontSize: 14)),
                        onPressed: () => {_submit(data['name'] ?? 'Student')},
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.green),
                            shape: MaterialStateProperty.all<OutlinedBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25))))),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                    ),
                    OutlinedButton(
                      child: Text(
                        'Cancel  ',
                        style: TextStyle(
                            fontFamily: 'Monsterrat',
                            color: Colors.black,
                            fontSize: 14),
                      ),
                      onPressed: () => {_cancel()},
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.red),
                          shape: MaterialStateProperty.all<OutlinedBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)))),
                    )
                  ],
                ));
            return SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [studentCard, dateCard, timeCard, buttonCard]));
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      resizeToAvoidBottomInset: false,
    );
  }

  @override
  void initState() {
    getUserID();
    super.initState();
  }

  void getUserID() async {
    String id = await HelperFunctions.getUserEmailSharedPreference();
    String name = await HelperFunctions.getUserNameSharedPreference();
    if (mounted) {
      setState(() {
        _instructorID = id;
        _instructorName = name;
      });
    }
  }
}
