import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/screens/edit_profile.dart';
import '../constants.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/screens/availability.dart';

class CreateBio extends StatefulWidget {
  @override
  _CreateBio createState() => _CreateBio();

  final String _password;
  Map<String, dynamic> userInfoMap = {};

  CreateBio(this.userInfoMap, this._password);
}

class _CreateBio extends State<CreateBio> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController _bio = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    void _submit() async {
      if (_bio.text.isNotEmpty) {
        widget.userInfoMap['bio'] = _bio.text;
      }

      Constants.myEmail = widget.userInfoMap['email'];
      Constants.myName = widget.userInfoMap['name'];
      HelperFunctions.saveUserNameSharedPreference(widget.userInfoMap['name']);
      HelperFunctions.saveUserEmailSharedPreference(
          widget.userInfoMap['email']);
      HelperFunctions.saveUserIsInstructorSharedPreference(
          widget.userInfoMap['instructor']);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  Availability(widget.userInfoMap, widget._password, false)));
    }

    Widget textCard = Column(
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 25),
          child: Text(
            'Add Bio',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 30,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
          ),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 0),
          child: Text(
            'Tell your students a little about yourself',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 14,
              color: Colors.black,
            ),
          ),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            'This is what students looking for instructors will read',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 14,
              color: Colors.grey[600],
            ),
          ),
        )
      ],
    );

    Widget textEditorCard = Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(),
        ),
        maxLines: null,
        textAlign: TextAlign.left,
        controller: _bio,
        keyboardType: TextInputType.multiline,
      ),
    );

    //Widget confirmSkip = ;

    Widget buttonCard = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
            height: 40,
            width: 100,
            child: OutlinedButton(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.white)),
              child: Text(
                'Skip',
                style: TextStyle(fontSize: 15, color: Colors.blue[900]),
              ),
              //creates AlertDialog to confirm skip
              onPressed: () async {
                BuildContext dialogContext;
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      dialogContext = context;
                      return AlertDialog(
                        title: Text("Are you sure?",
                            style: TextStyle(
                              fontSize: 20,
                            )),
                        content: Text(
                          "You can always edit your Bio under your profile later.",
                        ),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.pop(dialogContext);
                              },
                              child: Text("Cancel",
                                  style: TextStyle(
                                    fontSize: 17,
                                  ))),
                          TextButton(
                              onPressed: () {
                                Navigator.pop(dialogContext);
                                _submit();
                              },
                              child: Text("Confirm",
                                  style: TextStyle(
                                    fontSize: 17,
                                  )))
                        ],
                      );
                    },
                    barrierDismissible: true);
              },
            )),
        SizedBox(width: 20),
        SizedBox(
            height: 40,
            width: 100,
            child: OutlinedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.lightBlue[50])),
                onPressed: () async {
                  _submit();
                },
                child: Text(
                  'Submit',
                  style: TextStyle(fontSize: 15, color: Colors.blue[900]),
                )))
      ],
    );

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: primaryBar('Create Bio', true),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).size.height * 0.1),
              textCard,
              textEditorCard,
              SizedBox(height: 30.0),
              buttonCard,
              Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom)),
            ],
          ),
        ));
  }
}
