import 'package:flutter/material.dart';
import 'package:interlude_music_app/screens/instructor_pin.dart';

import 'create_acct.dart';
import 'sign_in.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          fontFamily: 'Montserrat',
          textTheme: TextTheme(
            bodyText1: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        home: Scaffold(
            body: Center(
                child: Stack(children: <Widget>[
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/conductor.jpg'),
                  fit: BoxFit.cover,
                  colorFilter:
                      ColorFilter.mode(Colors.white, BlendMode.multiply)),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.22),
                  height: MediaQuery.of(context).size.height * 0.12,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child:
                      /*FractionallySizedBox(
                      widthFactor: 1,
                      heightFactor: 0.4,
                      child: FractionallySizedBox(
                        widthFactor: 0.7,
                        heightFactor: 0.3,
                        child: */
                      Image(
                    image: AssetImage('assets/interludetitlewhite.png'),
                  ),
                  //),
                  //),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.09),
                      child: Text(
                        '       Welcome!\nAre you a Student\n      or Instructor\n',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Montserrat',
                          fontSize: 25,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0),
                      child: OutlinedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CreateAccount(false)));
                        },
                        child: Text(
                          'Student',
                          style: TextStyle(
                            color: Colors.white70,
                            fontFamily: 'Montserrat',
                            fontSize: 18,
                          ),
                        ),
                        style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          side: BorderSide(width: 1, color: Colors.white70),
                          padding: EdgeInsets.fromLTRB(50.0, 20.0, 50.0, 20.0),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0),
                      child: OutlinedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VerifyInstructor()));
                        },
                        child: Text(
                          'Instructor',
                          style: TextStyle(
                            color: Colors.white70,
                            fontFamily: 'Montserrat',
                            fontSize: 18,
                          ),
                        ),
                        style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          side: BorderSide(width: 1, color: Colors.white70),
                          padding: EdgeInsets.fromLTRB(43.0, 20.0, 43.0, 20.0),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.03),
                      child: TextButton(
                        child: Text('Already have an account? Login',
                            style: TextStyle(
                              fontFamily: 'Monsterrat',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              decoration: TextDecoration.underline,
                            )),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignIn(),
                                  fullscreenDialog: true));
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ]))));
  }
}
