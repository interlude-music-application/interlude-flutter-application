import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/constants.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/notification.dart';
import 'package:interlude_music_app/screens/lessons.dart';
import 'package:interlude_music_app/screens/single_chat.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';

import 'bottom_nav.dart';
import 'confirm_email.dart';
import 'main_drawer.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _isStudent = true;
  String _profileID;
  String _docID;
  String timezone;
  List lessonList;
  int nextLesson;
  QuerySnapshot snapshot;

  final DatabaseMethods databaseMethods = new DatabaseMethods();
  NotificationService notificationService;

  Future<void> isEmailVerified() async {
    // If the user has signed in but their email is not verified, redirect
    // to email confirmation page
    if (FirebaseAuth.instance.currentUser != null) {
      await FirebaseAuth.instance.currentUser.reload();
      // print(FirebaseAuth.instance.currentUser.emailVerified);
      if (!FirebaseAuth.instance.currentUser.emailVerified) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ConfirmEmail(), fullscreenDialog: true));
      }
    }
  }

  Future<void> setUpInteractedMessage() async {
    // If the user has opened the app by pressing a notification, redirect to
    // the appropriate page

    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null && initialMessage.data['type'] == 'chat') {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SingleChat(
                  initialMessage.data['receivedByEmail'],
                  initialMessage.data['sendByEmail'],
                  initialMessage.data['sendByName'])));
    } else if (initialMessage != null &&
        initialMessage.data['type'] == 'lessonStatus') {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => Lessons()));
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      if (message.data['type'] == 'chat') {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SingleChat(
                    message.data['receivedByEmail'],
                    message.data['sendByEmail'],
                    message.data['sendByName'])));
      } else if (message.data['type'] == 'lessonStatus') {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Lessons()));
      }
    });
  }

  @override
  void initState() {
    isStudent();
    getUserInfo();
    isEmailVerified();
    setUpInteractedMessage();
    super.initState();
  }

  void isStudent() {
    HelperFunctions.getUserIsInstructorSharedPreference().then((isInstructor) {
      if (mounted) {
        setState(() {
          _isStudent = !isInstructor;
        });
      }
    });
  }

  updateInfo() {
    databaseMethods.updateUserInfo(_docID, {
      "chattingWith": "",
    });
  }

  getUserInfo() {
    HelperFunctions.getUserEmailSharedPreference().then((email) {
      if (mounted) {
        setState(() {
          _profileID = email;
          Constants.myEmail = email;
        });
      }

      databaseMethods.getUserByUserEmail(_profileID).then((val) {
        if (mounted) {
          setState(() {
            if (val.docs.length == 0) return;

            _docID = val.docs[0].id;

            if (val.docs[0].data()["total_hours"] != null) {
              Constants.hours = val.docs[0].data()["total_hours"].toDouble();
            }
            if (val.docs[0].data()["name"] != null) {
              Constants.myName = val.docs[0].data()["name"];
            }
            if (val.docs[0].data()["timezone"] != null) {
              timezone = val.docs[0].data()["timezone"];
            }
          });
        }
      });

      updateInfo();

      notificationService =
          new NotificationService(_profileID, context, !_isStudent);
      notificationService.initialize();

      getLessons();
    });
  }

  int getNextLesson(list) {
    DateTime now = DateTime.now();
    int index = -1;
    if (list == null) return index;

    for (var i = 0; i < list.length; i++) {
      if (now.isBefore(list[i].data()["endTime"].toDate())) {
        if (list[i].data()["status"] == "Upcoming") {
          index = i;
        }
      } else {
        break;
      }
    }

    return index;
  }

  getLessons() {
    databaseMethods
        .getLessonsUser(_profileID, _isStudent, true, false)
        .then((val) {
      if (mounted) {
        setState(() {
          snapshot = val;
        });
      }

      lessonList = List.empty(growable: true);
      if (snapshot != null) {
        for (var i = 0; i < snapshot.docs.length; i++) {
          Map<String, dynamic> data = snapshot.docs[i].data();
          if (data["status"] == "Upcoming") {
            lessonList.add(snapshot.docs[i]);
          }
        }
      }
    });
  }

  Widget ofHoursText(String text) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.04,
      child: FittedBox(
        fit: BoxFit.fill,
        child: Text(text),
      ),
    );
  }

  Widget main() {
    final endIndex = Constants.myName.indexOf(" ");
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.35,
          child: FittedBox(
            fit: BoxFit.fill,
            child: Text(
              "Welcome,",
            ),
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.08,
            child: FittedBox(
              fit: BoxFit.fill,
              child: Text(
                endIndex != -1
                    ? Constants.myName.substring(0, endIndex) + "!"
                    : Constants.myName + "!",
              ),
            ),
          ),
        ),
        Container(
          padding:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
          child: Stack(
            alignment: Alignment.center,
            children: [
              CircularPercentIndicator(
                radius: MediaQuery.of(context).size.width * 0.6,
                lineWidth: MediaQuery.of(context).size.width * 0.03,
                percent: Constants.hours < 5
                    ? Constants.hours / 5
                    : Constants.hours < 10
                        ? Constants.hours / 10
                        : Constants.hours < 20
                            ? Constants.hours / 20
                            : Constants.hours < 40
                                ? Constants.hours / 40
                                : Constants.hours < 60
                                    ? Constants.hours / 60
                                    : Constants.hours < 100
                                        ? Constants.hours / 100
                                        : Constants.hours < 200
                                            ? Constants.hours / 200
                                            : 100 / 100,
                circularStrokeCap: CircularStrokeCap.round,
                progressColor: Colors.blue[800],
                animation: true,
                animationDuration: 1000,
              ),
              Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.12,
                    child: FittedBox(
                      fit: BoxFit.fill,
                      child: Text(
                        Constants.hours.toInt().toString(),
                      ),
                    ),
                  ),
                  Constants.hours < 5
                      ? ofHoursText("of 5 hours")
                      : Constants.hours < 10
                          ? ofHoursText("of 10 hours")
                          : Constants.hours < 20
                              ? ofHoursText("of 20 hours")
                              : Constants.hours < 40
                                  ? ofHoursText("of 40 hours")
                                  : Constants.hours < 60
                                      ? ofHoursText("of 60 hours")
                                      : Constants.hours < 100
                                          ? ofHoursText("of 100 hours")
                                          : ofHoursText("of 200 hours")
                ],
              )
            ],
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.75,
          margin: EdgeInsets.only(left: 4),
          padding: EdgeInsets.all(15),
          child: nextLesson != -1
              ? Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.08,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: Text(
                          '${DateFormat.Md().format(lessonList[nextLesson].data()["startTime"].toDate())}',
                          style: TextStyle(
                              fontFamily: 'Monsterrat', color: Colors.black),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: Text(
                          '${DateFormat.jm().format(lessonList[nextLesson].data()["startTime"].toDate())}-${DateFormat.jm().format(lessonList[nextLesson].data()["endTime"].toDate())} ${lessonList[nextLesson].data()["startTime"].toDate().timeZoneName}',
                          style: TextStyle(
                              fontFamily: 'Monsterrat', color: Colors.black),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.04,
                      child: FittedBox(
                          fit: BoxFit.fill,
                          child: Text(
                            'Your Next Lesson',
                            style: TextStyle(
                                fontFamily: 'Monsterrat', color: Colors.black),
                          )),
                    ),
                  ],
                )
              : SizedBox(
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: FittedBox(
                      fit: BoxFit.fill,
                      child: Text(
                        'No upcoming \n lessons are \n currently scheduled',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Monsterrat',
                          color: Colors.black,
                        ),
                      )),
                ),
        ),
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    nextLesson = getNextLesson(lessonList);
    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: Image.asset('assets/interludetitleblack.png',
              fit: BoxFit.contain, scale: 5.0),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        automaticallyImplyLeading: true,
      ),
      drawer: MainDrawer(),
      body: main(),
      bottomNavigationBar: BottomNav(1, false),
    );
  }
}
