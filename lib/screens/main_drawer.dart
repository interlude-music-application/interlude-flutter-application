import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/authenticate/auth.dart';
import 'package:interlude_music_app/constants.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/screens/instructors.dart';

import '../database.dart';
import 'about.dart';
import 'chat_room.dart';
import 'help.dart';
import 'home.dart';
import 'hours.dart';
import 'lessons.dart';
import 'prizes.dart';
import 'profile.dart';

class MainDrawer extends StatelessWidget {
  final AuthService _auth = AuthService();
  DatabaseMethods databaseMethods = new DatabaseMethods();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: FutureBuilder(
      future: isInstructor(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasError) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              "Oops! It looks like an error has occurred...",
            ),
          ));
          return Container();
        }

        if (snapshot.hasData) {
          bool _isInstructor = snapshot.data;
          return Drawer(
            child: Column(
              children: <Widget>[
                ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 7,
                    top: 7,
                    bottom: 7,
                  ),
                  leading: Icon(
                    Icons.account_box_rounded,
                    color: Colors.blue[800],
                    size: 50,
                  ),
                  title: Text(
                    Constants.myName.indexOf(' ') > -1
                        ? Constants.myName
                            .substring(0, Constants.myName.indexOf(' '))
                        : Constants.myName,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Profile()));
                  },
                ),
                Divider(
                  color: Colors.black26,
                  thickness: 2,
                  height: 1,
                ),
                ListTile(
                  leading: Icon(
                    Icons.home_sharp,
                    color: Colors.blue[800],
                    size: 30,
                  ),
                  title: Text(
                    'Home',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(
                    Icons.today_rounded,
                    color: Colors.blue[800],
                    size: 30,
                  ),
                  title: Text(
                    'Lessons',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Lessons()));
                  },
                ),
                ListTile(
                    leading: Icon(
                      Icons.chat,
                      color: Colors.blue[800],
                      size: 30,
                    ),
                    title: Text(
                      'Chats',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => ChatRoom()));
                    }),
                !_isInstructor
                    ? ListTile(
                        leading: Icon(
                          Icons.search,
                          color: Colors.blue[800],
                          size: 30,
                        ),
                        title: Text(
                          'Instructors',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SearchInstructors()));
                        })
                    : Container(),
                _isInstructor
                    ? ListTile(
                        leading: Icon(
                          Icons.schedule,
                          color: Colors.blue[800],
                          size: 30,
                        ),
                        title: Text(
                          'Hours',
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Hours()));
                        })
                    : Container(),
                ListTile(
                    leading: Icon(
                      Icons.emoji_events,
                      color: Colors.blue[800],
                      size: 30,
                    ),
                    title: Text(
                      'Prizes',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Prizes(
                                    hours: Constants.hours,
                                  )));
                    }),
                ListTile(
                  leading: Icon(
                    Icons.info,
                    color: Colors.blue[800],
                    size: 30,
                  ),
                  title: Text(
                    'About',
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => About()),
                    );
                  },
                ),
                ListTile(
                    leading: Icon(
                      Icons.help,
                      color: Colors.blue[800],
                      size: 30,
                    ),
                    title: Text(
                      'Help',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Help()));
                    }),
                Spacer(),
                Divider(
                  color: Colors.black26,
                  thickness: 2,
                  height: 1,
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 5,
                    bottom: 5,
                  ),
                  child: ListTile(
                    leading: Icon(
                      Icons.login,
                      color: Colors.black45,
                      size: 30,
                    ),
                    title: Text(
                      'Logout',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    onTap: () async {
                      // Removing firebase messaging token
                      // This prevents device from receiving push notifications
                      // after user is signed out
                      FirebaseMessaging messaging = FirebaseMessaging.instance;
                      String token = await messaging.getToken();
                      String userEmail =
                          await HelperFunctions.getUserEmailSharedPreference();

                      // Unsubscribe device from topic notifications
                      await FirebaseMessaging.instance
                          .unsubscribeFromTopic('general');

                      if (_isInstructor) {
                        await FirebaseMessaging.instance
                            .unsubscribeFromTopic('instructors');
                      } else {
                        await FirebaseMessaging.instance
                            .unsubscribeFromTopic('students');
                      }

                      databaseMethods.getUserByUserEmail(userEmail).then((doc) {
                        databaseMethods.updateUserInfo(doc.docs[0].id, {
                          "tokens": FieldValue.arrayRemove([token])
                        }).then((val) {
                          _auth.signOut().then((value) {
                            Navigator.popUntil(
                                context, ModalRoute.withName('/'));
                          });
                        });
                      });
                    },
                  ),
                ),
              ],
            ),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    ));
  }

  Future<bool> isInstructor() async {
    return await HelperFunctions.getUserIsInstructorSharedPreference();
  }
}
