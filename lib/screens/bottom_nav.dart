import 'package:flutter/material.dart';

import '../constants.dart';
import '../database.dart';
import 'chat_room.dart';
import 'home.dart';
import 'lessons.dart';

class BottomNav extends StatefulWidget {
  @override
  _BottomNavState createState() => _BottomNavState();

  //This is a hack to make the bar appear without a selected item; bottom nav bar must have an item selected to prevent out of index error
  final bool _disable;
  final int _index;

  BottomNav(this._index, this._disable);
}

class _BottomNavState extends State<BottomNav> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  Stream unreadMessageStream;

  @override
  void initState() {
    getUserStream();
    super.initState();
  }

  void getUserStream() {
    databaseMethods.getUserByUserEmailStream(Constants.myEmail).then((value) {
      if (mounted) {
        setState(() {
          // print("read the db");
          unreadMessageStream = value;
        });
      }
    });
  }

  Color unselect() {
    if (widget._disable == true) {
      return Color.fromRGBO(159, 155, 155, 1);
    } else {
      return Color.fromRGBO(76, 133, 199, 1);
    }
  }

  @override
  Widget build(BuildContext context) {
    void _onItemTapped(int index) {
      if (index != widget._index || widget._disable == true) {
        switch (index) {
          case 0:
            // if (_disable == true) break;
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Lessons()));
            break;
          case 1:
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Home()));
            break;
          case 2:
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => ChatRoom()));
            break;
        }
      }
    }

    return StreamBuilder(
      stream: unreadMessageStream,
      builder: (context, snapshot) {
        return snapshot.hasData && snapshot.data.docs.length != 0
            ? BottomNavigationBar(
                items: [
                  BottomNavigationBarItem(
                      icon: Icon(
                        Icons.today_rounded,
                        size: 30,
                      ),
                      label: 'Lessons'),
                  BottomNavigationBarItem(
                      icon: Icon(
                        Icons.home_sharp,
                        size: 30,
                      ),
                      label: 'Home'),
                  BottomNavigationBarItem(
                    icon: snapshot.data.docs[0].data()["unreadMessage"]
                        ? Icon(
                            Icons.mark_chat_unread_outlined,
                            color: Color.fromRGBO(76, 133, 199, 1),
                            size: 30,
                          )
                        : Icon(
                            Icons.chat,
                            size: 30,
                          ),
                    label: 'Chats',
                  )
                ],
                selectedItemColor: unselect(),
                unselectedItemColor: Color.fromRGBO(159, 155, 155, 1),
                currentIndex: widget._index,
                onTap: _onItemTapped,
                backgroundColor: Colors.white,
              )
            : BottomNavigationBar(
                items: [
                  BottomNavigationBarItem(
                      icon: Icon(
                        Icons.today_rounded,
                        size: 30,
                      ),
                      label: 'Lessons'),
                  BottomNavigationBarItem(
                      icon: Icon(
                        Icons.home_sharp,
                        size: 30,
                      ),
                      label: 'Home'),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.chat,
                      size: 30,
                    ),
                    label: 'Chats',
                  )
                ],
                selectedItemColor: unselect(),
                unselectedItemColor: Color.fromRGBO(159, 155, 155, 1),
                currentIndex: widget._index,
                onTap: _onItemTapped,
                backgroundColor: Colors.white,
              );
      },
    );
  }
}
