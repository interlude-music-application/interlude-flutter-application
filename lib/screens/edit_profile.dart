import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/screens/availability.dart';
import 'package:interlude_music_app/screens/profile.dart';

import '../constants.dart';
import 'edit_bio.dart';
import 'forgot_password.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfile createState() => _EditProfile();

  final String _docID;
  final bool _isInstructor;
  var data;
  EditProfile(this._docID, this._isInstructor, this.data);
}

class _EditProfile extends State<EditProfile> {
  final DatabaseMethods databaseMethods = new DatabaseMethods();

  String _email;
  bool _available;
  String _state;
  String _timezone;
  TextEditingController _name = TextEditingController();
  TextEditingController _grade = TextEditingController();
  TextEditingController _school = TextEditingController();
  TextEditingController _instruments = TextEditingController();
  TextEditingController _experience = TextEditingController();

  final _textFieldStlye = TextStyle(
    fontFamily: 'Monsterrat',
    fontSize: 12,
  );

  @override
  void initState() {
    loadUser();
    super.initState();
  }

  loadUser() async {
    String email = await HelperFunctions.getUserEmailSharedPreference();
    if (mounted) {
      setState(() {
        _email = email;
      });
    }
    getUserInfo();
  }

  getUserInfo() async {
    databaseMethods.getUserByUserEmail(_email).then((val) {
      if (mounted) {
        setState(() {
          if (val.docs[0].data()["available"] != null) {
            _available = val.docs[0].data()["available"];
          }
          if (val.docs[0].data()["state"] != null) {
            _state = val.docs[0].data()["state"];
          }

          if (val.docs[0].data()["timezone"] != null) {
            _timezone = val.docs[0].data()["timezone"];
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // ignore: non_constant_identifier_names
    List<String> instrument_arr() {
      List<String> instrum_arr = _instruments.text.split(", ");
      for (int i = 0; i < instrum_arr.length; i++) {
        instrum_arr[i] = instrum_arr[i].toLowerCase();
      }
      return instrum_arr;
    }

    _name = TextEditingController(text: widget.data["name"]);
    _grade = TextEditingController(text: widget.data["grade_level"]);
    _school = TextEditingController(text: widget.data["school"]);
    _instruments = TextEditingController(text: widget.data["instruments"]);
    _experience =
        TextEditingController(text: widget.data["years_of_experience"]);

    void _submit() async {
      Map<String, Object> userMap = new Map();

      if (_name.text.isNotEmpty) userMap['name'] = _name.text;
      if (_name.text.isNotEmpty)
        userMap['nameToLower'] = _name.text.toLowerCase();
      if (_state != widget.data["state"]) userMap['state'] = _state;
      if (_timezone != widget.data["timezone"]) userMap['timezone'] = _timezone;
      if (_grade.text.isNotEmpty) userMap['grade_level'] = _grade.text;
      if (_school.text.isNotEmpty) userMap['school'] = _school.text;
      if (_instruments.text.isNotEmpty)
        userMap['instruments'] = _instruments.text;
      userMap['instrument_arr'] = instrument_arr();
      if (_experience.text.isNotEmpty)
        userMap['years_of_experience'] = _experience.text;
      if (widget._isInstructor && _available != widget.data["available"])
        userMap['available'] = _available;

      if (userMap.isNotEmpty) {
        await databaseMethods.updateUserInfo(widget._docID, userMap);
        await HelperFunctions.saveUserNameSharedPreference(_name.text);
      }

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => Profile()));
    }

    void _cancel() async {
      Navigator.pop(context);
    }

    Widget titleCard = Container(
        padding: EdgeInsets.symmetric(vertical: 30, horizontal: 60),
        child: Text(
          'Edit Profile',
          style: TextStyle(
              fontFamily: 'Monsterrat', color: Colors.black, fontSize: 30),
        ));

    Widget editCard = Container(
      padding: EdgeInsets.symmetric(horizontal: 60),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'Full Name',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)))),
            controller: _name,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'State',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.07,
            child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton<String>(
                    value: _state,
                    icon: const Icon(Icons.expand_more),
                    iconSize: 24,
                    elevation: 16,
                    isExpanded: true,
                    underline: Container(
                      height: 0.5,
                      color: Colors.black,
                    ),
                    onChanged: (String newValue) {
                      if (mounted) {
                        setState(() {
                          _state = newValue;
                        });
                      }
                    },
                    items: Constants.states
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList())),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'Time Zone',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.07,
            child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton<String>(
                    value: _timezone,
                    icon: const Icon(Icons.expand_more),
                    iconSize: 24,
                    elevation: 16,
                    isExpanded: true,
                    underline: Container(
                      height: 0.5,
                      color: Colors.black,
                    ),
                    onChanged: (String newValue) {
                      if (mounted) {
                        setState(() {
                          _timezone = newValue;
                        });
                      }
                    },
                    items: Constants.timezones
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList())),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'Grade Level',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)))),
            controller: _grade,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'School',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)))),
            controller: _school,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'Instrument(s)',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          TextFormField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)))),
            controller: _instruments,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
            child: Text(
              'Years of Experience',
              style: _textFieldStlye,
              textAlign: TextAlign.start,
            ),
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(15),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)))),
            controller: _experience,
          ),
          (widget._isInstructor)
              ? Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Text("Accepting New Students"),
                  Switch(
                    value: _available,
                    onChanged: (value) {
                      if (mounted) {
                        setState(() {
                          _available = value;
                          // print(_available);
                        });
                      }
                    },
                    activeTrackColor: Colors.lightBlueAccent,
                    activeColor: Colors.blue,
                  ),
                ])
              : Container(),
          (widget._isInstructor)
              ? Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  TextButton(
                    child: Text('Availability',
                        style: TextStyle(
                          fontFamily: 'Monsterrat',
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          decoration: TextDecoration.underline,
                        )),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Availability(
                                Map<String, dynamic>(), widget._docID, true),
                          ));
                    },
                  ),
                  TextButton(
                    child: Text('Edit Bio',
                        style: TextStyle(
                          fontFamily: 'Monsterrat',
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          decoration: TextDecoration.underline,
                        )),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditBio(widget._docID,
                                widget._isInstructor, widget.data),
                          ));
                    },
                  ),
                ])
              : Container(),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            TextButton(
              child: Text('Change Password',
                  style: TextStyle(
                    fontFamily: 'Monsterrat',
                    fontSize: 12,
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    decoration: TextDecoration.underline,
                  )),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ForgotPassword(
                              emailID: widget.data["email"],
                            ),
                        fullscreenDialog: true));
              },
            ),
          ]),
        ],
      ),
    );

    Widget buttonCard = Container(
        padding: EdgeInsets.only(top: 10),
        margin: EdgeInsets.only(bottom: 30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlinedButton(
                child: Text('Update',
                    style: TextStyle(
                        fontFamily: 'Monsterrat',
                        color: Colors.black,
                        fontSize: 14)),
                onPressed: () => {_submit()},
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.green),
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25))))),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
            ),
            OutlinedButton(
              child: Text(
                'Cancel',
                style: TextStyle(
                    fontFamily: 'Monsterrat',
                    color: Colors.black,
                    fontSize: 14),
              ),
              onPressed: () => {_cancel()},
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                  shape: MaterialStateProperty.all<OutlinedBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25)))),
            )
          ],
        ));

    return Scaffold(
      appBar: primaryBar('My Profile', false),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [titleCard, editCard, buttonCard]),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
