import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/constants.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/screens/single_chat.dart';

import 'bottom_nav.dart';
import 'main_drawer.dart';

class ChatRoom extends StatefulWidget {
  @override
  _ChatRoomState createState() => _ChatRoomState();

  // If these optional parameters are provided,
  // immediately reroute to a single chatroom
  final String userName;
  final String userEmail;
  ChatRoom([this.userName, this.userEmail]);
}

class _ChatRoomState extends State<ChatRoom> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController searchTextEditingController =
      new TextEditingController();

  bool _isInstructor;
  Stream chatRoomsStream;

  QuerySnapshot searchSnapshot;

  initiateSearch() {
    String searchText = searchTextEditingController.text;
    databaseMethods.getUserThroughSearch(searchText, _isInstructor).then((val) {
      if (mounted) {
        setState(() {
          searchSnapshot = val;
        });
      }
    });
  }

  // create chatroom, send user to conversation screen, pushreplacement
  createChatroomAndStartConversation(String userName, String userEmail) {
    if (userEmail != Constants.myEmail) {
      String chatRoomId = getChatRoomId(userEmail, Constants.myEmail);
      Constants.chat = userName;

      List<String> users = [userName, Constants.myName];
      List<String> usersID = [userEmail, Constants.myEmail];

      Map<String, dynamic> chatRoomMap = {
        "users": users,
        "usersID": usersID,
        "chatroomId": chatRoomId
      };

      DatabaseMethods().createChatRoom(chatRoomId, chatRoomMap);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SingleChat(Constants.myEmail, userEmail)));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("You cannot send a message to yourself"),
      ));
    }
  }

  Widget searchList() {
    return searchSnapshot != null
        ? Expanded(
            child: ListView.separated(
            physics: BouncingScrollPhysics(),
            itemCount: searchSnapshot.docs.length,
            itemBuilder: (content, index) {
              Map<String, dynamic> data = searchSnapshot.docs[index].data();

              return searchTile(
                userName: data["name"],
                userEmail: data["email"],
              );
            },
            separatorBuilder: (BuildContext context, int index) => Divider(
              color: Theme.of(context).scaffoldBackgroundColor,
              height: 5,
            ),
          ))
        : Expanded(
            child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [chatRoomList()],
            ),
          ));
  }

  Widget searchTile({String userName, String userEmail}) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 7),
                  child: Text(userName,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                ),
                Text(userEmail)
              ],
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                createChatroomAndStartConversation(userName, userEmail);
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.blue[700],
                    borderRadius: BorderRadius.circular(30)),
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Text(
                  "Message",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ));
  }

  Widget chatRoomList() {
    return StreamBuilder(
      stream: chatRoomsStream,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: snapshot.data.docs.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  Map<String, dynamic> data = snapshot.data.docs[index].data();

                  List<dynamic> users = data["users"];
                  List<dynamic> usersID = data["usersID"];
                  String otherUser, otherEmail;
                  users.forEach((user) {
                    if (user != Constants.myName) {
                      otherUser = user;
                    }
                  });
                  usersID.forEach((userID) {
                    if (userID != Constants.myEmail) {
                      otherEmail = userID;
                    }
                  });
                  return ChatRoomTile(
                    userName: otherUser,
                    sendByEmail: Constants.myEmail,
                    receivedByEmail: otherEmail,
                    lastMessage: data["lastMessage"],
                  );
                })
            : Container();
      },
    );
  }

  @override
  void initState() {
    try {
      String chatroomId = getChatRoomId(widget.userEmail, Constants.myEmail);
      databaseMethods.doesChatRoomExist(chatroomId).then((exists) {
        if (exists) {
          Constants.chat = widget.userName;
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SingleChat(Constants.myEmail, widget.userEmail)));
        } else {
          createChatroomAndStartConversation(widget.userName, widget.userEmail);
        }
      });
    } catch (error) {}

    getUserInfo();
    super.initState();
  }

  getUserInfo() async {
    databaseMethods.getUserByUserEmail(Constants.myEmail).then((value) {
      if (mounted) {
        setState(() {
          if (value == null) return;

          if (value.docs[0].data()["name"] != null)
            Constants.myName = value.docs[0].data()["name"];

          if (value.docs[0].data()["instructor"] != null)
            _isInstructor = value.docs[0].data()["instructor"];
        });
      }
    });

    databaseMethods.getChatRooms(Constants.myEmail).then((value) {
      if (mounted) {
        setState(() {
          chatRoomsStream = value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Chats', true),
      drawer: MainDrawer(),
      body: Container(
          child: Column(children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
          child: Row(children: [
            Expanded(
              child: TextField(
                controller: searchTextEditingController,
                decoration: InputDecoration(
                    hintText: "Search Users...", border: InputBorder.none),
              ),
            ),
            GestureDetector(
              onTap: () {
                initiateSearch();
              },
              child: Icon(
                Icons.search,
                size: 30,
              ),
            ),
          ]),
        ),
        Divider(
          color: Colors.black26,
          thickness: 1,
        ),
        searchList()
      ])),
      bottomNavigationBar: BottomNav(2, false),
    );
  }
}

class ChatRoomTile extends StatelessWidget {
  final String userName;
  final String sendByEmail;
  final String receivedByEmail;
  final String lastMessage;

  ChatRoomTile(
      {this.userName,
      this.sendByEmail,
      this.receivedByEmail,
      this.lastMessage});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Constants.chat = userName;
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SingleChat(sendByEmail, receivedByEmail)));
        },
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: CircleAvatar(
                      radius: 25,
                      child: Image(image: AssetImage('assets/Ellipse.png'))),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                        child: Text(
                          userName != null ? userName : 'Username',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 24, bottom: 16),
                          child: Text(
                            lastMessage != null ? lastMessage : 'Message',
                            maxLines: 1,
                            // softWrap: false,
                            overflow: TextOverflow.ellipsis,
                          )),
                    ],
                  ),
                ),
                SizedBox(width: 25.0),
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: IconButton(
                    icon: Icon(Icons.keyboard_arrow_right),
                    onPressed: () {
                      Constants.chat = userName;
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  SingleChat(sendByEmail, receivedByEmail)));
                    },
                  ),
                )
              ],
            ),
            Divider(
              color: Colors.black26,
              thickness: 1,
            )
          ],
        ));
  }
}
