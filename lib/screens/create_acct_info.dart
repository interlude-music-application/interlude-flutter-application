import 'package:flutter/material.dart';

import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:interlude_music_app/screens/availability.dart';
import 'create_acct_bio.dart';

import '../constants.dart';
import '../ui/sharedWidgets/customText.dart';

class CreateAccountInfo extends StatefulWidget {
  final bool _instructor;
  final String _email;
  final String _password;
  Map<String, dynamic> userInfoMap = {};
  CreateAccountInfo(this._email, this._password, this._instructor); //const

  @override
  _CreateAccountInfoState createState() => _CreateAccountInfoState();
}

class _CreateAccountInfoState extends State<CreateAccountInfo> {
  final _formKey = GlobalKey<FormState>();
  DatabaseMethods databaseMethods = new DatabaseMethods();

  // text field state

  final _name = TextEditingController();
  String _state = "AK";
  String _timezone = "PDT";
  final _grade = TextEditingController();
  final _school = TextEditingController();
  final _instruments = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Container(
            child: Image.asset('assets/interludetitleblack.png',
                fit: BoxFit.contain, scale: 5.0),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          automaticallyImplyLeading: true,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);
              if (!currentFocus.hasPrimaryFocus &&
                  currentFocus.focusedChild != null) {
                currentFocus.focusedChild.unfocus();
              }
            },
            child: Padding(
                padding: EdgeInsets.only(bottom: bottom),
                child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    reverse: true,
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 60.0),
                        child: Form(
                          key: _formKey,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: size.height * 0.1),
                                Center(
                                  child: Text(
                                    'Create Account',
                                    style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontSize: 30,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                SizedBox(height: size.height * 0.02),
                                customText('Full Name'),
                                Container(
                                  height: size.height * 0.05,
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.fromLTRB(15, 20, 10, 0),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20.0)),
                                      ),
                                    ),
                                    validator: (val) =>
                                        val.isEmpty ? 'Enter your name' : null,
                                    controller: _name,
                                  ),
                                ),
                                SizedBox(height: size.height * 0.02),
                                customText('State'),
                                Container(
                                  height: size.height * 0.05,
                                  child: ButtonTheme(
                                      alignedDropdown: true,
                                      child: DropdownButton<String>(
                                          value: _state,
                                          icon: const Icon(Icons.expand_more),
                                          iconSize: 24,
                                          elevation: 16,
                                          isExpanded: true,
                                          underline: Container(
                                            height: 0.5,
                                            color: Colors.black,
                                          ),
                                          onChanged: (String newValue) {
                                            if (mounted) {
                                              setState(() {
                                                _state = newValue;
                                              });
                                            }
                                          },
                                          items: Constants.states
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList())),
                                ),
                                SizedBox(height: size.height * 0.02),
                                customText('Time Zone'),
                                Container(
                                  height: size.height * 0.05,
                                  child: ButtonTheme(
                                      alignedDropdown: true,
                                      child: DropdownButton<String>(
                                          value: _timezone,
                                          icon: const Icon(Icons.expand_more),
                                          iconSize: 24,
                                          elevation: 16,
                                          isExpanded: true,
                                          underline: Container(
                                            height: 0.5,
                                            color: Colors.black,
                                          ),
                                          onChanged: (String newValue) {
                                            if (mounted) {
                                              setState(() {
                                                _timezone = newValue;
                                              });
                                            }
                                          },
                                          items: Constants.timezones
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList())),
                                ),
                                SizedBox(height: size.height * 0.02),
                                customText('Grade Level'),
                                Container(
                                  height: size.height * 0.05,
                                  child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.fromLTRB(15, 20, 10, 0),
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20.0)),
                                        ),
                                      ),
                                      validator: (val) => val.isEmpty
                                          ? 'Enter your grade'
                                          : null,
                                      controller: _grade),
                                ),
                                SizedBox(height: size.height * 0.02),
                                customText('School'),
                                Container(
                                  height: size.height * 0.05,
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.fromLTRB(15, 20, 10, 0),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20.0)),
                                      ),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? 'Enter your school'
                                        : null,
                                    controller: _school,
                                  ),
                                ),
                                SizedBox(height: size.height * 0.04),
                                customText('Instrument(s)'),
                                Container(
                                  height: size.height * 0.05,
                                  child: TextFormField(
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      hintText: "Ex. Piano, Violin",
                                      suffixIcon: Tooltip(
                                          preferBelow: false,
                                          message:
                                              "Use , to separate instruments",
                                          child: Icon(
                                            Icons.error_outlined,
                                            color: Colors.blue,
                                          )),
                                      contentPadding:
                                          EdgeInsets.fromLTRB(15, 20, 10, 0),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20.0))),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? 'Enter your instrument(s)'
                                        : null,
                                    controller: _instruments,
                                  ),
                                ),
                                SizedBox(height: size.height * 0.02),
                                Center(
                                  child: OutlinedButton(
                                    child: Text(
                                      'Next',
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.blue[900]),
                                    ),
                                    onPressed: () async {
                                      if (_formKey.currentState.validate()) {
                                        //creates instrument_arr var
                                        List<String> instrument_arr =
                                            _instruments.text.split(", ");
                                        for (int i = 0;
                                            i < instrument_arr.length;
                                            i++) {
                                          instrument_arr[i] =
                                              instrument_arr[i].toLowerCase();
                                        }

                                        Map<String, dynamic> userInfoMap = {
                                          "name": _name.text,
                                          "nameToLower":
                                              _name.text.toLowerCase(),
                                          "email": widget._email,
                                          "unreadMessage": false,
                                          "state": _state,
                                          "timezone": _timezone,
                                          "total_hours": 0,
                                          "grade_level": _grade.text,
                                          "school": _school.text,
                                          "instruments": _instruments.text,
                                          "instructor": widget._instructor,
                                          "instrument_arr": instrument_arr
                                        };

                                        if (widget._instructor) {
                                          userInfoMap["available"] = true;
                                          userInfoMap["hours"] = {};

                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      CreateBio(userInfoMap,
                                                          widget._password)));
                                        } else {
                                          Constants.myEmail = widget._email;
                                          Constants.myName = _name.text;
                                          HelperFunctions
                                              .saveUserNameSharedPreference(
                                                  _name.text);
                                          HelperFunctions
                                              .saveUserEmailSharedPreference(
                                                  widget._email);
                                          HelperFunctions
                                              .saveUserIsInstructorSharedPreference(
                                                  widget._instructor);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      Availability(
                                                          userInfoMap,
                                                          widget._password,
                                                          false)));
                                        }
                                      }
                                    },
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.lightBlue[50])),
                                  ),
                                ),
                                SizedBox(height: 12.0),
                              ]),
                        ))))));
  }
}
