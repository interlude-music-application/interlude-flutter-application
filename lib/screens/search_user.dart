import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/screens/schedule_lesson.dart';

class SearchUser extends StatefulWidget {
  @override
  _SearchUser createState() => _SearchUser();
}

class _SearchUser extends State<SearchUser> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController searchTextEditingController =
      new TextEditingController();

  QuerySnapshot searchSnapshot;

  initiateSearch() {
    String searchText = searchTextEditingController.text;
    databaseMethods.getUserThroughSearch(searchText, true).then((val) {
      if (mounted) {
        setState(() {
          searchSnapshot = val;
        });
      }
    });
  }

  Widget searchList() {
    return searchSnapshot != null
        ? Expanded(
            child: ListView.separated(
            physics: BouncingScrollPhysics(),
            itemCount: searchSnapshot.docs.length,
            itemBuilder: (content, index) {
              Map<String, dynamic> data = searchSnapshot.docs[index].data();
              // print(data);
              return searchTile(
                userName: data["name"],
                userEmail: data["email"],
              );
            },
            separatorBuilder: (BuildContext context, int index) => Divider(
              color: Theme.of(context).scaffoldBackgroundColor,
              height: 5,
            ),
          ))
        : Container();
  }

  Widget searchTile({String userName, String userEmail}) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(bottom: 7),
                  child: Text(userName,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                ),
                Text(userEmail)
              ],
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Schedule(userEmail)));
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.blue[700],
                    borderRadius: BorderRadius.circular(30)),
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Text(
                  "Schedule",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar('Search User', true),
      body: Container(
          child: Column(children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
          child: Row(children: [
            Expanded(
              child: TextField(
                controller: searchTextEditingController,
                decoration: InputDecoration(
                    hintText: "Search Users...", border: InputBorder.none),
              ),
            ),
            GestureDetector(
              onTap: () {
                initiateSearch();
              },
              child: Icon(
                Icons.search,
                size: 30,
              ),
            ),
          ]),
        ),
        Divider(
          color: Colors.black26,
          thickness: 1,
        ),
        searchList()
      ])),
    );
  }
}
