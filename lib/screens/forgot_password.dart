import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/ui/sharedWidgets/customText.dart';

class ForgotPassword extends StatefulWidget {
  static String id = 'forgot-password';
  final String message =
      "An email has just been sent to you, Click the link provided to complete your password reset";
  final String emailID;

  ForgotPassword({this.emailID});

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final _auth = FirebaseAuth.instance;

  final _formKey = GlobalKey<FormState>();
  final _email = TextEditingController();

  String email = '';

  _passwordReset() async {
    try {
      _formKey.currentState.save();
      await _auth.sendPasswordResetEmail(email: email);

      // Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
    } catch (e) {
      // print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    (widget.emailID != null) ? email = widget.emailID : email = '';

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Container(
            child: Image.asset('assets/interludetitleblack.png',
                fit: BoxFit.contain, scale: 5.0),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          automaticallyImplyLeading: true,
        ),
        body: Container(
          height: size.height * 0.93,
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: size.height * 0.02),
                Center(
                  child: Text(
                    'Reset Password',
                    style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w500,
                      fontSize: 30,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(height: size.height * 0.08),
                if (widget.emailID == null) customText('Username'),
                if (widget.emailID == null)
                  Container(
                    height: 40,
                    child: TextFormField(
                        controller: _email,
                        decoration: InputDecoration(
                          suffixIcon: Icon(Icons.person),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 15.0),
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                          ),
                        ),
                        validator: (val) =>
                            val.isEmpty ? 'Enter an email' : null,
                        onChanged: (val) {
                          if (mounted) {
                            setState(() => email = val);
                          }
                        }),
                  ),
                if (widget.emailID == null)
                  SizedBox(height: size.height * 0.04),
                Center(
                  child: Container(
                    height: 40,
                    width: 107,
                    child: SizedBox(
                      width: double.infinity,
                      child: OutlinedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.lightBlue[50])),
                          child: Text('Send Email',
                              style: TextStyle(
                                  fontSize: 15, color: Colors.blue[900])),
                          onPressed: () {
                            _passwordReset();
                            // print(_email);
                          }),
                    ),
                  ),
                ),
                /*Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        child: Text('Send Email'),
                        onPressed: () {
                          _passwordReset();
                          // print(_email);
                        },
                      ),
                    ])*/
              ],
            ),
          ),
        ),
      ),
    );
  }
}
