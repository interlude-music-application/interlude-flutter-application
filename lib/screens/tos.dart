// terms of service popup widget

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'create_acct_info.dart';

Future<void> tosAgreement(BuildContext context, String email, String password,
    bool instructor) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Terms of Service'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              RichText(
                  text: TextSpan(children: [
                TextSpan(
                  text:
                      'By creating an account, you agree to our Terms of Service. \n',
                  style: new TextStyle(color: Colors.black, fontSize: 16),
                ),
                TextSpan(
                    text: "Learn More.",
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () async {
                        const url = "https://www.interludeoutreach.org/tos";
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(
                              "This URL could not be launched",
                            ),
                          ));
                        }
                      }),
              ])),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Back'),
            style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.grey)),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text('Continue'),
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          CreateAccountInfo(email, password, instructor)));
              ;
            },
          ),
        ],
      );
    },
  );
}
