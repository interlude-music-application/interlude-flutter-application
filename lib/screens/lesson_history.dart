import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/helperfunctions.dart';
import 'package:intl/intl.dart';

import 'lessons.dart';

class LessonHistory extends StatefulWidget {
  @override
  _LessonHistoryState createState() => _LessonHistoryState();
}

class _LessonHistoryState extends State<LessonHistory> {
  DatabaseMethods databaseMethods = new DatabaseMethods();
  TextEditingController searchTextEditingController =
      new TextEditingController();

  String _userID = "";
  bool _isStudent = false;
  List<Map<String, dynamic>> _docs = List.empty(growable: true);
  QuerySnapshot snapshot;

  getLessons() {
    databaseMethods
        .getLessonsUser(_userID, _isStudent, false, false)
        .then((val) {
      if (mounted) {
        setState(() {
          snapshot = val;
        });
      }

      if (snapshot != null) {
        _docs = List.empty(growable: true);
        for (var i = 0; i < snapshot.docs.length; i++) {
          Map<String, dynamic> data = snapshot.docs[i].data();
          _docs.add(data);
        }
      }
    });
  }

  Widget lessonList() {
    return snapshot != null && _docs.length != 0 && snapshot.docs.length != 0
        ? ListView.builder(
            itemCount: _docs.length,
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (content, index) {
              String date =
                  '${DateFormat.yMMMd().format(_docs[_docs.length - 1 - index]["startTime"].toDate())}';
              String time =
                  '${DateFormat.jm().format(_docs[_docs.length - 1 - index]["startTime"].toDate())}-${DateFormat.jm().format(_docs[_docs.length - 1 - index]["endTime"].toDate())} ${_docs[_docs.length - 1 - index]["startTime"].toDate().timeZoneName}';

              return _isStudent
                  ? LessonTile(
                      name: _docs[_docs.length - 1 - index]["instructor_name"],
                      date: date,
                      timeRange: time,
                      status: _docs[_docs.length - 1 - index]["status"])
                  : new GestureDetector(
                      onTap: () {},
                      child: LessonTile(
                          name: _docs[_docs.length - 1 - index]["student_name"],
                          date: date,
                          timeRange: time,
                          status: _docs[_docs.length - 1 - index]["status"]));
            })
        : Center(
            child: Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Text("No Lesson History",
                    style: TextStyle(
                        fontFamily: 'Monsterrat',
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: primaryBar("My Lesson History", true),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [lessonList()],
        ),
      ),
    );
  }

  @override
  void initState() {
    getUserID();
    super.initState();
  }

  void getUserID() async {
    String id = await HelperFunctions.getUserEmailSharedPreference();
    if (mounted) {
      setState(() {
        _userID = id;
        isStudent();
      });
    }
  }

  void isStudent() async {
    databaseMethods.getUserByUserEmail(_userID).then((val) {
      if (mounted) {
        setState(() {
          if (val.docs[0].data()["instructor"] != null) {
            _isStudent = !val.docs[0].data()["instructor"];
            getLessons();
          }
        });
      }
    });
  }
}
