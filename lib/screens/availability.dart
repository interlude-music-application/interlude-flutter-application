import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:interlude_music_app/app/theme/global.dart';
import 'package:interlude_music_app/authenticate/auth.dart';
import 'package:interlude_music_app/constants.dart';
import 'package:interlude_music_app/database.dart';
import 'package:interlude_music_app/screens/home.dart';
import 'package:interlude_music_app/screens/profile.dart';
import 'package:intl/intl.dart';
import 'package:weekday_selector/weekday_selector.dart';

import '../helperfunctions.dart';

class Availability extends StatefulWidget {
  final Map _userMap;
  final String _pass;
  final bool _update;
  const Availability(this._userMap, this._pass, this._update);
  @override
  _Availability createState() => _Availability();
}

class _Availability extends State<Availability> {
  final AuthService _auth = AuthService();
  DatabaseMethods databaseMethods = new DatabaseMethods();

  TimeOfDay _startTime = TimeOfDay(hour: 12, minute: 0);
  TimeOfDay _endTime = TimeOfDay(hour: 13, minute: 0);
  final _dayList = List.filled(7, false);

  TextEditingController _availability = TextEditingController();
  bool overwrite = false;
  String previousSchedule = "";

  void _generateString() {
    DateTime date = DateTime.now();
    String generated = "";

    final Map weekdayConst = {
      0: "Sun",
      1: "M",
      2: "Tu",
      3: "W",
      4: "Th",
      5: "F",
      6: "Sat"
    };

    //Get Days of the week
    int startIndex = -1;
    bool split = false;
    for (int i = 0; i < 7; i++) {
      if (_dayList[i] == true) {
        if (startIndex == -1) {
          startIndex = i;
          if (split == true) {
            generated += ", ";
          }
          generated += weekdayConst[i];
          split = true;
        } else if (i == 6) {
          generated += "-" + weekdayConst[i];
        }
      } else {
        if (startIndex != -1) {
          if ((i - 1) != startIndex) {
            generated += "-" + weekdayConst[i - 1];
          }
          startIndex = -1;
        }
      }
    }

    //Clear string when nothing is selected
    if (generated.isEmpty == true) {
      if (mounted) {
        setState(() {
          _availability.text = generated;
        });
      }
      return;
    }

    generated += " ";

    //Get starting time
    generated += DateFormat.jm().format(DateTime(date.year, date.month,
            date.day, _startTime.hour, _startTime.minute)) +
        "-";

    //Get ending time
    generated += DateFormat.jm().format(DateTime(
        date.year, date.month, date.day, _endTime.hour, _endTime.minute));

    //Get Time Zone
    generated += " " +
        DateTime(date.year, date.month, date.day, _startTime.hour,
                _startTime.minute)
            .timeZoneName;

    if (mounted) {
      setState(() {
        _availability.text = generated;
      });
    }
  }

  Future<void> _selectTime(BuildContext context, bool start) async {
    TimeOfDay selected;
    if (start) {
      selected = await showTimePicker(
          context: context,
          initialTime: _startTime,
          initialEntryMode: TimePickerEntryMode.input);
      if (selected != null && selected != _startTime) {
        if (mounted) {
          setState(() {
            _startTime = selected;
          });
        }
      }
    } else {
      selected = await showTimePicker(
          context: context,
          initialTime: _endTime,
          initialEntryMode: TimePickerEntryMode.input);
      if (selected != null && selected != _endTime) {
        if (mounted) {
          setState(() {
            _endTime = selected;
          });
        }
      }
    }
    _generateString();
  }

  @override
  void initState() {
    // Save User Preferences
    Constants.myEmail = widget._userMap["email"];
    Constants.myName = widget._userMap["name"];
    HelperFunctions.saveUserNameSharedPreference(widget._userMap["name"]);
    HelperFunctions.saveUserEmailSharedPreference(widget._userMap["email"]);
    HelperFunctions.saveUserIsInstructorSharedPreference(
        widget._userMap["instructor"]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget textCard = Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 30),
      child: Text(
        'Availability',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: 'Montserrat',
          fontSize: 30,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
      ),
    );

    Widget dayCard = Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Days of Availability',
                style: TextStyle(
                    fontFamily: 'Monsterrat',
                    color: Colors.black,
                    fontSize: 14),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 3),
              ),
              WeekdaySelector(
                onChanged: (int day) {
                  if (mounted) {
                    setState(() {
                      final index = day % 7;
                      _dayList[index] = !_dayList[index];
                    });
                  }
                  _generateString();
                },
                values: _dayList,
                firstDayOfWeek: 0,
              )
            ]));

    Widget timeCard = Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Hours of Availability',
                style: TextStyle(
                    fontFamily: 'Monsterrat',
                    color: Colors.black,
                    fontSize: 14),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 3),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Flexible(
                      fit: FlexFit.tight,
                      child: TextButton(
                        child: Text(
                          _startTime.format(context),
                          style:
                              TextStyle(fontFamily: 'Monsterrat', fontSize: 24),
                        ),
                        onPressed: () => _selectTime(context, true),
                      )),
                  Text('to',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Monsterrat',
                          color: Colors.black,
                          fontSize: 14)),
                  Flexible(
                      fit: FlexFit.tight,
                      child: TextButton(
                        child: Text(_endTime.format(context),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontFamily: 'Monsterrat', fontSize: 24)),
                        onPressed: () => _selectTime(context, false),
                      ))
                ],
              )
            ]));

    Widget outputCard = Container(
        child: Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
      child: TextField(
        textAlign: TextAlign.center,
        controller: _availability,
        keyboardType: TextInputType.text,
        enabled: false,
      ),
    ));

    Widget buttonCard = OutlinedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color>(Colors.lightBlue[50])),
        onPressed: () async {
          Map userInfoMap = widget._userMap;

          try {
            QuerySnapshot snapshot =
                await databaseMethods.getUserByUserEmail(Constants.myEmail);
            previousSchedule = snapshot.docs[0]["availability"] + "/n";
          } catch (error) {
            previousSchedule = "";
          }

          // The newline character should be \n not /n but firebase
          // doesn't suport newline characters, so it's being replaced when
          // accessed with actual newline characters
          // print(previousSchedule);
          if (!overwrite)
            userInfoMap["availability"] = previousSchedule + _availability.text;
          else
            userInfoMap["availability"] = _availability.text;

          if (widget._update == false) {
            dynamic result = await _auth.registerWithEmailAndPassword(
                userInfoMap["email"], widget._pass);

            if (result == null) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                  "Please Supply a Valid Email Address",
                ),
              ));
            } else {
              await databaseMethods.uploadUserInfo(userInfoMap);

              _auth.sendEmailVerification();
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Home()));
            }
          } else {
            await databaseMethods.updateUserInfo(widget._pass, userInfoMap);
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Profile()));
          }
        },
        child: Text(
          !widget._update ? 'Sign Up' : 'Add',
          style: TextStyle(fontSize: 15, color: Colors.blue[900]),
        ));

    Widget checkboxCard = CheckboxListTile(
      title: Text("Overwrite Saved Availability?"),
      value: overwrite,
      onChanged: (newValue) {
        if (mounted) {
          setState(() {
            overwrite = newValue;
          });
        }
      },
      controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: primaryBar('Availability', true),
      body: Column(
        children: [
          textCard,
          dayCard,
          timeCard,
          outputCard,
          buttonCard,
          !widget._update ? Container() : checkboxCard,
        ],
      ),
    );
  }
}
