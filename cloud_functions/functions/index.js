const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp();

const firestore = admin.firestore();
const messaging = admin.messaging();

//!!--HELPER FUNCTIONS --!!
//-------------------------
const addHours = async (userID, secID, dur) => {
    const userRef = firestore.collection('users');

    //Update instructor hours
    var snapshot = await userRef.where('email', '==', userID).get();

    if (snapshot.empty) {
        // console.log('No matching documents found!');
        return;
    }

    var doc = snapshot.docs[0];
    var data = doc.data();

    var hourCount = 0
    var totalHourCount = 0;
    var hourMap = {};

    //Check if total hours exist in user doc
    if ('total_hours' in data) totalHourCount = data.total_hours;
    totalHourCount += dur;

    //Check if hour map exist
    if ('hours' in data) {
        hourMap = data.hours;
        if (secID in hourMap) hourCount = hourMap[secID];
    }
    hourCount += dur;
    hourMap[secID] = hourCount;

    userRef.doc(doc.id).update({ total_hours: totalHourCount, hours: hourMap });

    //Update student hours
    snapshot = await userRef.where('email', '==', secID).get();

    if (snapshot.empty) {
        // console.log('No matching documents found!');
        return;
    }

    totalHourCount = 0;
    doc = snapshot.docs[0];
    data = doc.data();

    if ('total_hours' in data) totalHourCount = data.total_hours;
    totalHourCount += dur;

    userRef.doc(doc.id).update({ total_hours: totalHourCount });

}

const getTimeZoneName = (tz) => {
    switch (tz) {
        case "EDT":
            return "America/New_York";
        case "CDT":
            return "America/Chicago";
        case "MDT":
            return "America/Denver";
        case "AKDT":
            return "America/Anchorage";
        case "HST":
            return "Pacific/Honolulu";
        default:
            return "America/Los_Angeles";
    }
}

const sendNotification = async (message, tokens) => {
    // console.log(tokens);
    targetMessage = message;

    tokens.forEach(token => {
        targetMessage.token = token;
        // console.log(token);

        messaging.send(targetMessage).then((response) => {
            // console.log('Successfully sent notification:', response);
        }).catch((error) => {
            // console.log('Error sending message:', error);
        })
    });

    // console.log("Finished Notification send");
    return 0;
}


//!!--CLOUD FUNCTIONS --!!
//-------------------------

//Cloud function to automate completion of lessons and hour tracking
exports.completeLessons = functions.pubsub.schedule('every 5 minutes').onRun(async (context) => {
    const currentTime = admin.firestore.Timestamp.now();
    const lessonsRef = firestore.collection('lessons');
    const snapshot = await lessonsRef.where('status', '==', 'Upcoming').get();

    if (snapshot.empty) {
        // console.log('No matching documents found!');
        return;
    }

    snapshot.forEach(async (doc) => {
        const data = doc.data();
        const endTime = data.endTime;

        if (endTime < currentTime) {
            //Update lesson status
            lessonsRef.doc(doc.id).update({ status: 'Completed' });
            //Increase hour count
            await addHours(data.instructor, data.student, data.duration);
        }
    });

    // console.log("completeLessons has run");
    return 0;
});

exports.lessonStatusNotification = functions.firestore.document('lessons/{lessonID}').onWrite((change, context) => {
    // console.log('lessonStatus has begun');
    const data = (!change.after.exists) ? change.before.data() : change.after.data();

    var startTime = data.startTime.toDate();
    var endTime = data.endTime.toDate();
    let dateTimeOptions = { hour: '2-digit', minute: '2-digit', month: 'short', day: '2-digit' };
    let timeOptions = { hour: '2-digit', minute: '2-digit' };

    if (data.status == "Upcoming" || data.status == "Canceled") {
        // Send to student only
        student_tz = getTimeZoneName(data.student_timezone);
        // console.log(data.student_timezone);
        // console.log(student_tz);

        message = {
            notification: {
                title: 'Lesson ' + data.status,
                body: startTime.toLocaleString('en-US', Object.assign({}, dateTimeOptions, { timeZone: student_tz })) + ' - ' +
                    endTime.toLocaleTimeString('en-US', Object.assign({}, timeOptions, { timeZone: student_tz }))
            },
            data: {
                type: 'lessonStatus',
            }
        }
        sendNotification(message, data.student_tokens);
        // console.log(message);
        // console.log(data.student);
    } else {
        // Send to student and instructor
        student_tz = getTimeZoneName(data.student_timezone);
        instructor_tz = getTimeZoneName(data.instructor_timezone);
        // console.log(data.student_timezone);
        // console.log(data.instructor_timezone);
        // console.log(student_tz);
        // console.log(instructor_tz);

        student_message = {
            notification: {
                title: 'Lesson ' + data.status,
                body: startTime.toLocaleString('en-US', Object.assign({}, dateTimeOptions, { timeZone: student_tz })) + ' - ' +
                    endTime.toLocaleTimeString('en-US', Object.assign({}, timeOptions, { timeZone: student_tz }))
            },
            data: {
                type: 'lessonStatus',
            }
        }

        instructor_message = {
            notification: {
                title: 'Lesson ' + data.status,
                body: startTime.toLocaleString('en-US', Object.assign({}, dateTimeOptions, { timeZone: instructor_tz })) + ' - ' +
                    endTime.toLocaleTimeString('en-US', Object.assign({}, timeOptions, { timeZone: instructor_tz }))
            },
            data: {
                type: 'lessonStatus',
            }
        }
        sendNotification(student_message, data.student_tokens);
        sendNotification(instructor_message, data.instructor_tokens);
        // console.log(student_message);
        // console.log(instructor_message);
        // console.log(data.student);
        // console.log(data.instructor);
    }
    // console.log("lessonStatusNotification has run");
    return 0;
});

exports.newMessageNotification = functions.firestore.document('ChatRoom/{ChatRoomID}/chats/{chatID}').onCreate((snap, context) => {
    const data = snap.data();
    const message = data.message;
    const from = data.sendBy;
    const receivedByTokens = data.receivedByTokens;
    const sendByEmail = data.sendByID;
    const receivedByName = data.receivedBy;
    const receivedByEmail = data.receivedByID;

    notification = {
        notification: {
            title: from,
            body: message,
        },
        data: {
            type: 'chat',
            sendByEmail: sendByEmail,
            sendByName: from,
            receivedByEmail: receivedByEmail,
        }
    }
    // console.log(notification);
    sendNotification(notification, receivedByTokens);
    return 0;
});
